package at.htlstp.infofox_android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;
import java.util.NoSuchElementException;

import at.htlstp.infofox_android.database.InfoFoxSQLHelper;
import at.htlstp.infofox_android.gcm.GradeInstanceIDListenerService;
import at.htlstp.infofox_android.gcm.RegistrationIntentService;
import at.htlstp.infofox_android.models.ErrorMessage;
import at.htlstp.infofox_android.models.LoginData;
import at.htlstp.infofox_android.models.Token;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.services.TokenService;
import at.htlstp.infofox_android.util.ErrorParser;
import at.htlstp.infofox_android.util.TokenCallback;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * This is the inital activity of the application.
 * It presents the user with a login prompt, in which
 * he can enter his edv number and password. If the user
 * is already logged in, the activity forwards the user
 * to the maintabactivity.
 */
public class LoginActivity extends Activity {

    public final static String LOG_TAG = LoginActivity.class.getSimpleName();

    private SharedPreferences sp;

    private EditText email;
    private EditText passwort;
    private TextView skip;
    private ScrollView scrollView;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = getSharedPreferences(Constants.EDV_NUMMER, MODE_PRIVATE);

        //Initialize the Database with the context
        InfoFoxSQLHelper.initialize(this.getApplicationContext());

        /**
         * Try to initialize the Token from the Database.
         * If it works the user is currently logged in.
         * He will then be forwarded to the maintabactivity.
         */
        try {
            TokenUtil.initializeFromDatabase(this.getApplicationContext());
            Intent i = new Intent(this, MainTabActivity.class);
            //If there was a notification, forward it
            if(this.getIntent().getStringExtra(Constants.GRADES) != null){
                i.putExtra(Constants.GRADES, this.getIntent().getStringExtra(Constants.GRADES));
            }
            this.startActivity(i);

        } catch(NoSuchElementException e){
            //Do nothing
        }

        setContentView(R.layout.activity_login);

        email = (EditText) findViewById(R.id.login_email);
        passwort = (EditText) findViewById(R.id.login_passwort);
        scrollView = (ScrollView) findViewById(R.id.login_scrollview);
        loginButton = (Button) findViewById(R.id.login_button);
        skip = (TextView) findViewById(R.id.skip);

        /**
         * If the user does not want to log in,
         * and skips the login, he will be redirected to
         * the SkipActivity.
         */
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, SkipActivity.class);
                startActivity(i);
            }
        });

        /**
         * If the user was previously logged in put his edv
         * number on the screen and set the focus on the email.
         */
        email.setText(sp.getString(Constants.EDV_NUMMER, ""));
        email.requestFocus();

        /**
         * If the user presses on the enter button,
         * we will execute the login Method.
         */
        passwort.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    onLoginButtonClicked(null);
                    return true;
                }
                return false;
            }
        });

        //Set different Typefaces, because android changes the typeface if the input is a passwordfield
        passwort.setTypeface(Typeface.DEFAULT);
        passwort.setTransformationMethod(new PasswordTransformationMethod());
    }

    /**
     * Try to log in the user,
     * if the login was successful, redirect him
     * to the maintabactivity and save the token in the database.
     * Also save the user name in the shared preferences, so he
     * can be remembered.
     */
    public void onLoginButtonClicked(View v) {

        loginButton.setClickable(false);

        //Show a progressdialog
        final ProgressDialog bar = new ProgressDialog(this);
        bar.setIndeterminate(true);
        bar.setCancelable(false);
        bar.setMessage(getResources().getString(R.string.login_ladescreen));
        bar.show();

        //Initalize the Tokenutil with a token
        TokenUtil.initialize(this.getApplicationContext(), email.getText().toString(), passwort.getText().toString(), new TokenCallback<Token>() {

            @Override
            public void onSuccess(Response<Token> response) {
                SharedPreferences.Editor ed = sp.edit();
                ed.putString(Constants.EDV_NUMMER, email.getText().toString());
                ed.commit();

                Intent i = new Intent(LoginActivity.this, MainTabActivity.class);
                LoginActivity.this.startActivity(i);
                //Start google Notification Service
                startService(new Intent(LoginActivity.this, RegistrationIntentService.class));
                bar.dismiss();
                loginButton.setClickable(true);
            }

            @Override
            public void onError(ErrorMessage message) {
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_logindaten_falsch), Toast.LENGTH_LONG).show();
                bar.dismiss();
                loginButton.setClickable(true);
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
                bar.dismiss();
                loginButton.setClickable(true);
            }
        });

    }

    /**
     * Scrolls the view to the buttom,
     * if the user clicks on the edittext field.
     */
    public void onEditTextClicked(View v) {
        scrollView.fullScroll(ScrollView.FOCUS_DOWN);
        email.requestFocus();
    }

    /**
     * Return back to the home screen.
     */
    @Override
    public void onBackPressed(){
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        startActivity(i);
    }
}
