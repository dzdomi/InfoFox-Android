package at.htlstp.infofox_android;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import at.htlstp.infofox_android.database.InfoFoxSQLHelper;
import at.htlstp.infofox_android.fragments.nachhilfe.TutoringFragment;
import at.htlstp.infofox_android.fragments.nachhilfe.TutoringRootFragment;
import at.htlstp.infofox_android.fragments.teacher.TeacherFragment;
import at.htlstp.infofox_android.fragments.vmm.VmmRootFragment;
import at.htlstp.infofox_android.services.GCMTokenService;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * This activity holds the main view of the application.
 * Therefore it uses an ViewPager with 3 tabs. Each of those tabs
 * holds the fragment for the different functions of the application.
 * These 3 fragments are for VMM, Tutoring and Teacher information.
 * This viewpager is coupled with the ActionBarTabs, so that the
 * user is able to swipe and also press the tabs on the application bar.
 * In order to get to this view, the user has to log on the Login
 * activity or be already logged in with a valid session token from the
 * InfoFox-API.
 */
public class MainTabActivity extends AppCompatActivity {

    public static final String LOG_TAG = MainTabActivity.class.getSimpleName();

    //Holds the different tabs
    private TabLayout tabLayout;

    /**
     * Icons can be placed on each of the tabs in the tablayout.
     * These Icons can not be resized, so instead of icons we need to add a custom view
     * for each of these tabs. In this app these views are imagesviews,
     * which hold the images for the tabs.
     */
    private ImageView imagesFromTab[];

    //Holds the tabimages (gray), if a view was unselected
    private int imageNotSelected[] = {
            R.drawable.toolbar_vmm_logo,
            R.drawable.toolbar_nachhilfe,
            R.drawable.toolbar_lehrerinformation
    };

    //Holds the tabimages (orange), if a view was selected
    private int imageSelected[] = {
            R.drawable.toolbar_vmm_logo_orange,
            R.drawable.toolbar_nachhilfe_orange,
            R.drawable.toolbar_lehrerinformation_orange
    };

    /**
     * The ViewPager that will show each of the
     * corresponding fragments.
     */
    private ViewPager mViewPager;

    /**
     * We instantiate the different fragments for the different tabs,
     * so that we can use it in the FragmentStatePagerAdapter.
     */
    private VmmRootFragment vmmRootFragment = new VmmRootFragment();
    private TutoringRootFragment tutoringRootFragment = new TutoringRootFragment();
    private TeacherFragment teacherFragment = new TeacherFragment();

    /**
     * Used to access the context outside of the application scope.
     * Little hackish, but gets the job done.
     */
    private static Context mainContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tab);

        mainContext = this.getApplicationContext();

        setStatusBarColor();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);

        /**
         * This listener checks if the view is in the middle to show the FAB
         * for the tutoring fragment. So if tutoring is not displayed we have to animate
         * the FABs out. Otherwise we have to animate the buttons on the page. But this
         * should only work if the tutoring list is scrolled to the top.
         */
        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            private boolean isMid = false;

            /**
             * Checks if viewpager is in the middle or not.
             */
            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    isMid = true;
                } else {
                    isMid = false;
                }
            }

            /**
             * Checks the state of the viewpager, if Tutoring
             * was on top we have to animate the buttons in if the
             * scrolling finished. Otherwise we have to animate the
             * buttons out.
             */
            @Override
            public void onPageScrollStateChanged(int state) {
                if (!TutoringFragment.isWasOnTop()) {
                    return;
                }
                if (isMid) {
                    if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                        TutoringFragment.scaleButtonsOut(0);
                    } else if (state == ViewPager.SCROLL_STATE_SETTLING) {
                        TutoringFragment.scaleButtonsIn(300);

                    }
                } else {
                    if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                        TutoringFragment.scaleButtonsIn(300);
                    }
                }

            }
        });


        tabLayout = (TabLayout) findViewById(R.id.tabs);

        //Sets the Tabs for the whole view of the tablayout
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Sync the Viewpager with the tablayout
        tabLayout.setupWithViewPager(mViewPager);

        //Now we inflate the different custom views for the tabs
        imagesFromTab = new ImageView[3];
        for (int i = 0; i < 3; i++) {
            View view = LayoutInflater.from(this).inflate(R.layout.main_tabs, null);
            imagesFromTab[i] = (ImageView) view.findViewById(R.id.main_tab);
            imagesFromTab[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), imageNotSelected[i]));
            tabLayout.getTabAt(i).setCustomView(view);
        }
        //Set vmm as the standard tab
        imagesFromTab[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), imageSelected[0]));

        /**
         * If a tab was selected we have to swap the fragment from the
         * viewpager and set the image for the corresponding tab.
         */
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                imagesFromTab[tab.getPosition()].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), imageSelected[tab.getPosition()]));
                //Swap the fragment
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                imagesFromTab[tab.getPosition()].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), imageNotSelected[tab.getPosition()]));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                imagesFromTab[tab.getPosition()].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), imageSelected[tab.getPosition()]));
            }
        });

        //If a notification was received we have to cancel all from infofox
        if (this.getIntent().getStringExtra(Constants.GRADES) != null) {
            this.getIntent().removeExtra(Constants.GRADES);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancelAll();
            //Set vmm as the current item
            mViewPager.setCurrentItem(0);
        }
    }

    /**
     * Sets the menu bar of the app.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_tab, menu);
        return true;
    }

    /**
     * Handles clicks on the menubar.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        /**
         * If the user logs out, we have to destroy all data
         * stored in the database, tokens and also unregister for the notifications.
         * In any response from the server we have to logout from our app.
         */
        if (id == R.id.settings_logout) {
            GCMTokenService service = RetrofitInstance.getInstance().getBuilder().create(GCMTokenService.class);
            service.deleteToken(InfoFoxSQLHelper.getInstance().getGCMToken()).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Response<Void> response, Retrofit retrofit) {}

                @Override
                public void onFailure(Throwable t) {}
            });
            TokenUtil.destroyInstance();
            InfoFoxSQLHelper.getInstance().deleteAllData();
            MainTabActivity.this.startActivity(new Intent(MainTabActivity.this, LoginActivity.class));

        } else if(id == R.id.settings_about){
            /**
             * Shows the about page of the app.
             * Therefore we inflate the view and make
             * the links clickable of the textviews.
             * Then this view will be diplayed on the screen
             * as an alert.
             */
            View aboutView = getLayoutInflater().inflate(R.layout.alert_about, null);

            TextView link = (TextView) aboutView.findViewById(R.id.about_link);
            link.setMovementMethod(LinkMovementMethod.getInstance());

            aboutView.findViewById(R.id.about_scroll).setVerticalScrollBarEnabled(false);

            link = (TextView)aboutView.findViewById(R.id.about_link_credit);
            link.setMovementMethod(LinkMovementMethod.getInstance());

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(aboutView);
            builder.show();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Changes the color of the status bar
     * to orange.
     */
    private void setStatusBarColor() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = this.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            w.setStatusBarColor(this.getResources().getColor(R.color.status_bar));
        }
    }

    /**
     * Determines in which tab the viewpager
     * currently holds and propagates the backbutton
     * press to the corresponding fragment, which handles the
     * press.
     */
    @Override
    public void onBackPressed() {
        switch (tabLayout.getSelectedTabPosition()) {
            case 0:
                vmmRootFragment.onBackPressed();
                break;
            case 1:
                tutoringRootFragment.onBackPressed();
                break;
            case 2:
                teacherFragment.onBackPressed();
                break;
        }
    }

    /**
     * A FragmentStatePagerAdapter that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return vmmRootFragment;
                case 1:
                    return tutoringRootFragment;
                case 2:
                    return teacherFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return imageSelected.length;
        }


    }

    public static Context getMaincontext() {
        return MainTabActivity.mainContext;
    }

}

