package at.htlstp.infofox_android.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Laufendenote;
import at.htlstp.infofox_android.models.Token;

/**
 * Saves the grades and the InfoFox token and the gcm token in the database.
 * Every time the structure of the database changes, the database version
 * number needs to be increased. At the moment there are two tables, which
 * hold the values for the grades and for the tokens.
 */
public class InfoFoxSQLHelper extends SQLiteOpenHelper {

    public static final String LOG_TAG = InfoFoxSQLHelper.class.getSimpleName();

    public static final String TABLE_GRADES = "grades";
    public static final String TABLE_TOKEN  = "tokens";

    public static final String COLUMN_ID                = "id";
    public static final String COLUMN_GEG_KURZBEZ       = "gegKurzbez";
    public static final String COLUMN_GEG_LANGBEZ       = "gegLangbez";
    public static final String COLUMN_GEG_TEACHER       = "teacher";
    public static final String COLUMN_LNT_BEMERKUNG     = "lntBemerkung";
    public static final String COLUMN_LNT_BEWERTUNG     = "lntBewertung";
    public static final String COLUMN_LNT_REALBEWERTUNG = "lntRealBewertung";
    public static final String COLUMN_LNT_SETZE_5       = "lntSetze5";
    public static final String COLUMN_LNT_TIMESTAMP     = "lntTimestamp";
    public static final String COLUMN_LNT_GELESEN       = "lntGelesen";

    public static final String COLUMN_TOKEN_KEY = "token_key";
    public static final String COLUMN_TOKEN_DATE = "token_date";
    //Token TYPE:
    //0 = InfoFox Token
    //1 = GCM Token
    public static final String COLUMN_TOKEN_TYPE = "token_type";

    private static final String DATABASE_NAME = "infofox.db";
    private static final int DATABASE_VERSION = 14;

    private static InfoFoxSQLHelper db;

    public static InfoFoxSQLHelper getInstance() {
        return db;
    }

    public static void initialize(Context context){
        if(db == null){
            db = new InfoFoxSQLHelper(context);
        }
    }

    private InfoFoxSQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private InfoFoxSQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private InfoFoxSQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    /**
     * Creates the Database Schema with the tables
     * and the specified columns.
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        StringBuilder sb_grades = new StringBuilder();
        sb_grades.append("create table ");
        sb_grades.append(TABLE_GRADES);
        sb_grades.append(" (\n");
        sb_grades.append(COLUMN_ID);
        sb_grades.append(" integer primary key autoincrement, ");
        sb_grades.append(COLUMN_GEG_KURZBEZ);
        sb_grades.append(" text not null, ");
        sb_grades.append(COLUMN_GEG_LANGBEZ);
        sb_grades.append(" text not null, ");
        sb_grades.append(COLUMN_GEG_TEACHER);
        sb_grades.append(" text not null, ");
        sb_grades.append(COLUMN_LNT_BEMERKUNG);
        sb_grades.append(" text not null, ");
        sb_grades.append(COLUMN_LNT_BEWERTUNG);
        sb_grades.append(" text not null, ");
        sb_grades.append(COLUMN_LNT_REALBEWERTUNG);
        sb_grades.append(" real not null, ");
        sb_grades.append(COLUMN_LNT_SETZE_5);
        sb_grades.append(" integer not null, ");
        sb_grades.append(COLUMN_LNT_GELESEN);
        sb_grades.append(" integer not null, ");
        sb_grades.append(COLUMN_LNT_TIMESTAMP);
        sb_grades.append(" text not null, ");
        sb_grades.append("check ( ");
        sb_grades.append(COLUMN_LNT_SETZE_5);
        sb_grades.append(" IN (0,1)),");
        sb_grades.append("check ( ");
        sb_grades.append(COLUMN_LNT_GELESEN);
        sb_grades.append(" IN (0,1))");
        sb_grades.append(");");

        StringBuilder sb_token = new StringBuilder();
        sb_token.append("create table ");
        sb_token.append(TABLE_TOKEN);
        sb_token.append(" (\n");
        sb_token.append(COLUMN_TOKEN_KEY);
        sb_token.append(" text not null, ");
        sb_token.append(COLUMN_TOKEN_DATE);
        sb_token.append(" text, ");
        sb_token.append(COLUMN_TOKEN_TYPE);
        sb_token.append(" integer not null, ");
        sb_token.append("check ( ");
        sb_token.append(COLUMN_TOKEN_TYPE);
        sb_token.append(" IN (0,1))");
        sb_token.append(");");

        sqLiteDatabase.execSQL(sb_grades.toString());
        sqLiteDatabase.execSQL(sb_token.toString());

    }

    /**
     * This method gets called if the database version number
     * increases from the previous version of the db.
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_GRADES);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_TOKEN);
        onCreate(sqLiteDatabase);
    }

    /**
     * Returns the InfoFox token from the database.
     * Otherwise null will be returned.
     */
    public Token getInfoFoxToken(){
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_TOKEN);
        Cursor cursor = queryBuilder.query(db, null,COLUMN_TOKEN_TYPE + " = 0",null,null,null,null);
        Token token = null;
        while(cursor.moveToNext()) {
            token = new Token();
            token.setKey(cursor.getString(cursor.getColumnIndex(COLUMN_TOKEN_KEY)));
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
            try {
                token.setValid(sdf.parse(cursor.getString(cursor.getColumnIndex(COLUMN_TOKEN_DATE))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return token;
    }

    /**
     * Deletes the InfoFox token and inserts a new one.
     */
    public void deleteAndInsertInfoFoxToken(Token token){
        SQLiteDatabase db = this.getWritableDatabase();
        this.deleteInfoFoxToken();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TOKEN_KEY, token.getKey());
        Calendar cal = Calendar.getInstance();
        cal.setTime(token.getValid());
        values.put(COLUMN_TOKEN_DATE, cal.get(Calendar.DAY_OF_MONTH) + "." + cal.get(Calendar.MONTH) + "." + cal.get(Calendar.YEAR));
        values.put(COLUMN_TOKEN_TYPE, 0);
        db.insert(TABLE_TOKEN, null, values);
    }

    /**
     * Deletes the InfoFox token.
     */
    public void deleteInfoFoxToken(){
        this.getWritableDatabase().execSQL("DELETE FROM " + TABLE_TOKEN + " WHERE " + COLUMN_TOKEN_TYPE + " = 0");
    }

    /**
     * Returns the GCM token. Otherwise null.
     * @return
     */
    public String getGCMToken(){
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_TOKEN);
        Cursor cursor = queryBuilder.query(db, null,COLUMN_TOKEN_TYPE + " = 1",null,null,null,null);
        while(cursor.moveToNext()) {
            return cursor.getString(cursor.getColumnIndex(COLUMN_TOKEN_KEY));
        }
        return null;
    }

    /**
     * Deletes the GCM token and inserts a new one.
     */
    public void deleteAndInsertGCMToken(String token){
        SQLiteDatabase db = this.getWritableDatabase();
        this.deleteGCMToken();
        ContentValues values = new ContentValues();
        values.put(COLUMN_TOKEN_KEY, token);
        values.put(COLUMN_TOKEN_TYPE, 1);
        db.insert(TABLE_TOKEN, null, values);
    }

    /**
     * Deletes the GCM token.
     */
    public void deleteGCMToken(){
        this.getWritableDatabase().execSQL("DELETE FROM " + TABLE_TOKEN + " WHERE " + COLUMN_TOKEN_TYPE + " = 1");
    }

    /**
     * Returns all subjects, with the corresponding grades from
     * the database.
     */
    public List<Gegenstand> getAllSubjects(){
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_GRADES);
        Cursor cursor = queryBuilder.query(db, null,null,null,null,null,null);
        List<Gegenstand> gegenstaende = new ArrayList<>();
        while(cursor.moveToNext()) {
            Gegenstand g = new Gegenstand();
            g.setGegKurzbez(cursor.getString(cursor.getColumnIndex(COLUMN_GEG_KURZBEZ)));
            g.setGegLangbez(cursor.getString(cursor.getColumnIndex(COLUMN_GEG_LANGBEZ)));
            g.setTeacher(cursor.getString(cursor.getColumnIndex(COLUMN_GEG_TEACHER)));
            if (!gegenstaende.contains(g)) {
                g.setGrades(getAllGradesFromSubject(g));
                gegenstaende.add(g);
            }
        }
        return gegenstaende;
    }

    /**
     * Returns all grades from the given subject.
     */
    public List<Laufendenote> getAllGradesFromSubject(Gegenstand subject){
        SQLiteDatabase db = this.getReadableDatabase();
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_GRADES);
        Cursor cursor = queryBuilder.query(db, null,COLUMN_GEG_KURZBEZ + " = ? AND " + COLUMN_GEG_TEACHER + " = ?",new String[]{subject.getGegKurzbez(),subject.getTeacher()},null,null,null);
        List<Laufendenote> grades = new ArrayList<>();
        while(cursor.moveToNext()){
            Laufendenote note = new Laufendenote();
            note.setLntBemerkung(cursor.getString(cursor.getColumnIndex(COLUMN_LNT_BEMERKUNG)));
            note.setLntBewertung(cursor.getString(cursor.getColumnIndex(COLUMN_LNT_BEWERTUNG)));
            note.setLntRealBewertung(cursor.getDouble(cursor.getColumnIndex(COLUMN_LNT_REALBEWERTUNG)));
            note.setLntSetze5(cursor.getInt(cursor.getColumnIndex(COLUMN_LNT_SETZE_5)) == 1);
            note.setlntGelesen(cursor.getInt(cursor.getColumnIndex(COLUMN_LNT_GELESEN)) == 1);
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
            try {
                note.setLntTimestamp(sdf.parse(cursor.getString(cursor.getColumnIndex(COLUMN_LNT_TIMESTAMP))));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            grades.add(note);
        }
        return grades;
    }

    /**
     * Inserts a new grade to the given subject.
     */
    public void insertNewGrade(Gegenstand gegenstand, Laufendenote note){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        //Grades in the graphical view only have a maximum of 5.5
        if(note.getLntRealBewertung() > 5.5) {
            note.setLntRealBewertung(5.5);
        }

        values.put(COLUMN_GEG_KURZBEZ, gegenstand.getGegKurzbez());
        values.put(COLUMN_GEG_LANGBEZ, gegenstand.getGegLangbez());
        values.put(COLUMN_GEG_TEACHER, gegenstand.getTeacher());
        values.put(COLUMN_LNT_BEMERKUNG, note.getLntBemerkung());
        values.put(COLUMN_LNT_BEWERTUNG, note.getLntBewertung());
        values.put(COLUMN_LNT_REALBEWERTUNG, note.getLntRealBewertung());
        values.put(COLUMN_LNT_SETZE_5, note.getLntSetze5());
        values.put(COLUMN_LNT_GELESEN, note.getlntGelesen());
        Calendar cal = Calendar.getInstance();
        cal.setTime(note.getLntTimestamp());
        values.put(COLUMN_LNT_TIMESTAMP, cal.get(Calendar.DAY_OF_MONTH ) + "." + (cal.get(Calendar.MONTH)+1) + "." + cal.get(Calendar.YEAR));
        db.insert(TABLE_GRADES, null, values);
    }

    /**
     * Sets the read flag of the grade, so that no graphical
     * indicator is shown in the view.
     * @param g
     */
    public void updateRead(Gegenstand g){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_LNT_GELESEN, true);
        db.update(TABLE_GRADES,values,COLUMN_GEG_KURZBEZ + " = ? AND " + COLUMN_GEG_TEACHER + " = ?",new String[]{g.getGegKurzbez(),g.getTeacher()});
    }

    /**
     * Deletes all subjects and inserts the given subjects of the list and the method also inserts the
     * grades of the subjects to the database.
     */
    public void deleteAndInsertSubjects(List<Gegenstand> gegenstaende){
        this.deleteAllSubjects();
        for(Gegenstand g : gegenstaende){
            for(Laufendenote note : g.getGrades()) {
                insertNewGrade(g, note);
            }
        }

    }

    /**
     * Deletes all subjects.
     */
    public void deleteAllSubjects(){
        this.getWritableDatabase().delete(TABLE_GRADES, null, null);
    }

    /**
     * Deletes all grades of the given subject.
     */
    public void deleteSubject(Gegenstand gegenstand) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_GRADES, COLUMN_GEG_KURZBEZ + " = ? AND " + COLUMN_GEG_TEACHER + " = ?", new String[]{gegenstand.getGegKurzbez(), gegenstand.getTeacher()});
    }

    /**
     * Deletes all grades to the given subject and inserts the new grades.
     */
    public void deleteAndInsertGradesFromSubject(Gegenstand gegenstand, List<Laufendenote> list) {
        deleteSubject(gegenstand);
        for (Laufendenote l : list) {
            insertNewGrade(gegenstand, l);
        }
    }

    /**
     * Deletes all stored data in the database.
     * Use with CAUTION! Should only be used, if the
     * User logs out, or the session is expired.
     */
    public void deleteAllData(){
        deleteAllSubjects();
        deleteGCMToken();
        deleteInfoFoxToken();
    }
}
