package at.htlstp.infofox_android.fragments.vmm;


import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import at.htlstp.infofox_android.adapter.SubjectAdapter;
import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.database.InfoFoxSQLHelper;
import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Laufendenote;
import at.htlstp.infofox_android.models.Token;
import at.htlstp.infofox_android.services.GradeService;
import at.htlstp.infofox_android.util.AnimationUtil;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import xyz.hanks.library.SmallBang;

/**
 * Displays a recyclerview with all the subjects with an image
 * and the text of the subject.
 */
public class SubjectsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    private View view;
    private RecyclerView mRecyclerView;
    private LineChart subjectGraph;
    private ImageView subject_icon;

    private RelativeLayout chart_layout;
    private SwipeRefreshLayout swipeRefreshLayout;

    public SubjectsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view != null){
            return view;
        }

        view = inflater.inflate(R.layout.fragment_subjects, container, false);

        ScrollView scrollView = (ScrollView) view.findViewById(R.id.subjects_scrollview);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.subjects_swipe_refresh);
        subject_icon = (ImageView) view.findViewById(R.id.subjects_icon);
        subjectGraph = (LineChart) view.findViewById(R.id.subject_chart);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.subject_recycleview);
        chart_layout = (RelativeLayout) view.findViewById(R.id.subject_chart_layout);

        scrollView.setVerticalScrollBarEnabled(false);

        swipeRefreshLayout.setOnRefreshListener(this);
        subject_icon.setOnClickListener(this);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this.getActivity(), 3));
        //To get rid of the stupid error log message
        mRecyclerView.setAdapter(new SubjectAdapter(SubjectsFragment.this, new ArrayList<Gegenstand>()));

        //Need to be called this way, bug in Android App Combat
        //http://stackoverflow.com/questions/26858692/swiperefreshlayout-setrefreshing-not-showing-indicator-initially
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
            }
        });

        startGradeCheck();

        return view;

    }

    /**
     * Loads all subjects and their grades from the InfoFox-API
     * and stores the information in the database. It also
     * calculates the height for the recyclerview.
     */
    private void startGradeCheck() {

        GradeService service = RetrofitInstance.getInstance().getBuilder().create(GradeService.class);
        service.getGrades(TokenUtil.getInstance().getToken().getKey()).enqueue(new Callback<List<Gegenstand>>() {
            @Override
            public void onResponse(Response<List<Gegenstand>> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    List<Gegenstand> list = response.body();
                    InfoFoxSQLHelper.getInstance().deleteAndInsertSubjects(list);
                    mRecyclerView.invalidate();
                    mRecyclerView.setAdapter(new SubjectAdapter(SubjectsFragment.this, InfoFoxSQLHelper.getInstance().getAllSubjects()));
                    calculateAndSetRecyclerViewHeight();
                    setupChart(subjectGraph, getSubjectData());
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(SubjectsFragment.this.getActivity(), getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
                mRecyclerView.setAdapter(new SubjectAdapter(SubjectsFragment.this, InfoFoxSQLHelper.getInstance().getAllSubjects()));
                calculateAndSetRecyclerViewHeight();
                setupChart(subjectGraph, getSubjectData());
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        startGradeCheck();
    }

    /**
     * This method calculates the height of the recyclerview.
     * This is needed as the view is inside a scrollview and
     * should have a fixed height. It iterates trough every
     * item and adds for every third item the height value.
     */
    private void calculateAndSetRecyclerViewHeight(){
        //Dirty workaround, but otherwise the items will not get rendered
        mRecyclerView.getLayoutParams().height = 10000;
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                int height = 0;
                for (int i = 0; i < mRecyclerView.getAdapter().getItemCount(); i+=3) {
                    RecyclerView.ViewHolder holder = mRecyclerView.findViewHolderForAdapterPosition(i);
                    if(holder == null){
                        return;
                    }
                    height += holder.itemView.getMeasuredHeight();
                }
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mRecyclerView.getLayoutParams();
                lp.height = height;
                mRecyclerView.setLayoutParams(lp);
            }
        });
    }

    /**
     * Variables for the small easter egg.
     * Just show it every 15th time.
     */
    private int counter = 0;
    private int random = new Random().nextInt(15) + 1;
    private SmallBang bang;
    private boolean boom = false;

    @Override
    public void onClick(View v) {
        if(!boom && counter == random){
            counter = 0;
            if(bang == null){
                bang = SmallBang.attach2Window(this.getActivity());
                bang.setColors(new int[]{getResources().getColor(R.color.button_color_presses), getResources().getColor(R.color.explosion)});
            }
            random = new Random().nextInt(15) + 1;
            boom = true;
        } else if(!boom) {
            counter++;
        }

        /**
         * Expands or collapses the view.
         * And also sets the bang for the easter egg.
         */
        if(chart_layout.getVisibility() == View.GONE){
            subject_icon.setImageDrawable(getResources().getDrawable(R.drawable.arrow_blue));
            if(boom){
                bang.bang(subject_icon);
                boom = false;
            }

            AnimationUtil.expand(chart_layout, 300);
            subjectGraph.animateX(1500);
        } else {
            subject_icon.setImageDrawable(getResources().getDrawable(R.drawable.arrow_black));
            AnimationUtil.collapse(chart_layout, 300);
        }
    }

    /**
     * Sets up the graphical parts for the chart. Most of the
     * documentation can be found under the following link:
     * https://github.com/PhilJay/MPAndroidChart/wiki
     */
    private void setupChart(LineChart chart, LineData data) {

        chart.setDescription("");

        chart.setTouchEnabled(false);
        chart.setHighlightPerTapEnabled(true);
        chart.setPinchZoom(true);


        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setAvoidFirstLastClipping(true);
        chart.getXAxis().setDrawAxisLine(false);
        chart.getXAxis().setDrawLabels(false);

        chart.setData(data);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setEnabled(true);
        l.setWordWrapEnabled(true);
        l.setForm(Legend.LegendForm.CIRCLE);


        chart.getAxisRight().setEnabled(false);

        chart.getAxisLeft().setEnabled(true);
        chart.getAxisLeft().setInverted(true);
        chart.getAxisLeft().setAxisMaxValue(6.0f);
        chart.getAxisLeft().setAxisMinValue(0.0f);

    }

    /**
     * Creates a dataset for the current subjects.
     */
    private LineData getSubjectData() {

        List<LineDataSet> subject_data = new ArrayList<>();
        List<Gegenstand> subjects = InfoFoxSQLHelper.getInstance().getAllSubjects();
        int highest = 0;
        for(int i = 0; i < subjects.size(); i++){
            if(subjects.get(i).getGrades().size() == 1){
                continue;
            }
            List<Entry> grades = new ArrayList<>();
            for(int j = 0; j < subjects.get(i).getGrades().size(); j++){
                Laufendenote note = subjects.get(i).getGrades().get(j);
                float value = note.getLntRealBewertung().floatValue();
                grades.add(new Entry(value, j));
                if(j > highest){
                    highest = j;
                }
            }
            subject_data.add(setupLineDataSet(grades, subjects.get(i).getGegKurzbez(), subjects.get(i).getTeacher()));
        }

        //Add the maximal X-axis-entries for this graph
        List<String> xVals = new ArrayList<>();
        for(int i = 0; i <= highest; i++){
            xVals.add("" + i);
        }

        LineData data = new LineData(xVals, subject_data);
        return data;
    }

    /**
     * Sets up a list of entries for the given dataset,
     * mostly formatting and coloring.
     */
    private LineDataSet setupLineDataSet(List<Entry> entries, String subject_short, String teacher){
        LineDataSet set = new LineDataSet(entries, subject_short + "-" + teacher);
        set.setLineWidth(3f);
        set.setCircleSize(5.5f);
        Random rnd = new Random(subject_short.hashCode() + teacher.hashCode());
        int color = Color.rgb(rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        set.setColor(color);
        set.setCircleSize(0f);
        set.setDrawValues(false);
        return set;
    }

}
