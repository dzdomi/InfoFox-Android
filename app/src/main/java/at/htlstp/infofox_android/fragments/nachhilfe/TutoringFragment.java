package at.htlstp.infofox_android.fragments.nachhilfe;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import at.htlstp.infofox_android.MainTabActivity;
import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.adapter.NachhilfeAdapter;
import at.htlstp.infofox_android.models.ErrorMessage;
import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Klasse;
import at.htlstp.infofox_android.models.Kompetenz;
import at.htlstp.infofox_android.models.Nachhilfe;
import at.htlstp.infofox_android.services.ClassService;
import at.htlstp.infofox_android.services.SubjectService;
import at.htlstp.infofox_android.services.TutoringService;
import at.htlstp.infofox_android.util.ArrayAdapterUtil;
import at.htlstp.infofox_android.util.ErrorParser;
import at.htlstp.infofox_android.util.MultiSpinner;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * This fragment shows the home screen of the tutoring
 * part of the application. This fragment offers the possibility
 * to add tutoring, edit already offered tutoring and search for
 * tutoring after a given criteria.
 */
public class TutoringFragment extends Fragment implements View.OnClickListener {

    public static final String LOG_TAG = TutoringFragment.class.getSimpleName();

    private Context context;

    private View view;

    private ListView listview;
    private Spinner jahrgang_spinner;
    private Spinner subject_spinner;
    private MultiSpinner competence_spinner;
    private Button searchButton;
    private ProgressBar search_progressbar;
    private static FloatingActionButton edit_icon;
    private static FloatingActionButton add_icon;

    private Klasse klasseFromSchueler;
    private List<Gegenstand> availableSubjects = new ArrayList<>();
    private List<Nachhilfe> availableTutoring = new ArrayList<>();

    private NachhilfeAdapter adapter;

    private static boolean wasOnTop = true;

    public TutoringFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            setSpinnerToGegenstand();
            return view;
        }
        view = inflater.inflate(R.layout.fragment_nachhilfe, container, false);
        View header = inflater.inflate(R.layout.listview_header_nachhilfe_search, null);

        listview = (ListView) view.findViewById(R.id.nachhilfe_listview);
        edit_icon = (FloatingActionButton) view.findViewById(R.id.edit_icon);
        add_icon = (FloatingActionButton) view.findViewById(R.id.plus_icon);

        jahrgang_spinner = (Spinner) header.findViewById(R.id.jahrgang_spinner);
        subject_spinner = (Spinner) header.findViewById(R.id.subject_spinner);
        competence_spinner = (MultiSpinner) header.findViewById(R.id.competence_spinner);
        search_progressbar = (ProgressBar) header.findViewById(R.id.search_progress_bar);

        //Go to TutoringOfferFragment
        add_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = TutoringRootFragment.getManager().beginTransaction();
                TutoringOfferFragment naf = new TutoringOfferFragment();
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.replace(R.id.nachhilfe_root, naf, "nachhilfe");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack("nachhilfe");
                transaction.commit();
            }
        });

        //Go to TutoringEditFragment
        edit_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = TutoringRootFragment.getManager().beginTransaction();
                TutoringEditFragment nef = new TutoringEditFragment();
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.replace(R.id.nachhilfe_root, nef, "nachhilfe");
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.addToBackStack("nachhilfe");
                transaction.commit();
            }
        });

        searchButton = (Button) header.findViewById(R.id.searchButton);
        searchButton.setOnClickListener(this);

        ArrayAdapter<CharSequence> jahrgang_adapter = ArrayAdapter.createFromResource(this.getActivity(), R.array.jahrgang, android.R.layout.simple_spinner_item);
        jahrgang_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        jahrgang_spinner.setAdapter(jahrgang_adapter);

        //Load class for the user
        setSpinnerToKlasse();

        LoadCompetenceListener listener = new LoadCompetenceListener();
        jahrgang_spinner.setOnItemSelectedListener(listener);
        subject_spinner.setOnItemSelectedListener(listener);

        listview.addHeaderView(header);
        adapter = new NachhilfeAdapter(this.getContext(), availableTutoring, false);
        listview.setAdapter(adapter);
        listview.setVerticalScrollBarEnabled(false);

        //We have to notifiy the adapter if we actually scroll on the scrollview
        AbsListView.OnScrollListener onScrollListener = new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if (scrollState == SCROLL_STATE_TOUCH_SCROLL || scrollState == SCROLL_STATE_FLING) {
                    adapter.setIsMoving(true);
                } else {
                    adapter.setIsMoving(false);
                }
            }

            /**
             * If the view is on top, we have to scale the
             * buttons in and also scroll the buttons out
             * if we are not at the top.
             */
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (!view.canScrollVertically(-1)) {
                    if (!wasOnTop) {
                        scaleButtonsIn(0);
                        wasOnTop = true;
                    }
                } else {
                    if (wasOnTop) {
                        scaleButtonsOut(0);
                        wasOnTop = false;
                    }
                }
            }
        };

        listview.setOnScrollListener(onScrollListener);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public static void scaleButtonsIn(int offset) {
        Animation scaleIn = AnimationUtils.loadAnimation(MainTabActivity.getMaincontext(), R.anim.scale_fab_in);
        scaleIn.setStartOffset(offset);
        edit_icon.startAnimation(scaleIn);
        edit_icon.setVisibility(View.VISIBLE);
        add_icon.startAnimation(scaleIn);
        add_icon.setVisibility(View.VISIBLE);
    }

    public static void scaleButtonsOut(int offset) {
        Animation scaleOut = AnimationUtils.loadAnimation(MainTabActivity.getMaincontext(), R.anim.scale_fab_out);
        scaleOut.setStartOffset(offset);
        edit_icon.startAnimation(scaleOut);
        edit_icon.setVisibility(View.GONE);
        add_icon.startAnimation(scaleOut);
        add_icon.setVisibility(View.GONE);
    }

    public static boolean isWasOnTop() {
        return wasOnTop;
    }

    /**
     * If the user is logged in, the current class will be set to
     * the correct one.
     */
    private void setSpinnerToKlasse() {
        subject_spinner.setEnabled(false);
        List<Gegenstand> l = new ArrayList<>();
        l.add(0, new Gegenstand("Gegenstände werden geladen..."));
        subject_spinner.setAdapter(ArrayAdapterUtil.createAdapter(context, l));

        ClassService service = RetrofitInstance.getInstance().getBuilder().create(ClassService.class);
        service.getClassFromStudent(TokenUtil.getInstance().getToken().getKey()).enqueue(new Callback<Klasse>() {
            @Override
            public void onResponse(Response<Klasse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    klasseFromSchueler = response.body();
                    jahrgang_spinner.setSelection(Integer.parseInt(klasseFromSchueler.getKlaBezeichung().substring(0, 1)));
                    setSpinnerToGegenstand();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(TutoringFragment.this.getActivity(), getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Loads the subjects for the given class.
     */
    private void setSpinnerToGegenstand() {
        subject_spinner.setEnabled(false);
        List<Gegenstand> l = new ArrayList<>();
        l.add(0, new Gegenstand("Gegenstände werden geladen..."));
        subject_spinner.setAdapter(ArrayAdapterUtil.createAdapter(context, l));
        Integer year = null;
        if(jahrgang_spinner.getSelectedItemPosition() != 0){
            year = jahrgang_spinner.getSelectedItemPosition();
        }
        SubjectService service = RetrofitInstance.getInstance().getBuilder().create(SubjectService.class);
        service.getSubjectsByYear(TokenUtil.getInstance().getToken().getKey(),year).enqueue(new Callback<List<Gegenstand>>() {
            @Override
            public void onResponse(Response<List<Gegenstand>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    List<Gegenstand> l = response.body();
                    l.add(0, new Gegenstand("Keine Auswahl"));
                    subject_spinner.setEnabled(true);
                    subject_spinner.setAdapter(ArrayAdapterUtil.createAdapter(context, l));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(TutoringFragment.this.getActivity(), getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Tries to call the API with the search criteria provided.
     *
     */
    @Override
    public void onClick(View v) {
        searchButton.setEnabled(false);
        search_progressbar.setVisibility(View.VISIBLE);
        Gegenstand g = null;
        Integer year = null;
        List<Integer> k = null;
        Call<List<Nachhilfe>> serviceCall = null;

        if (subject_spinner.getSelectedItem() != null && subject_spinner.getSelectedItemPosition() != 0) {
            g = (Gegenstand) subject_spinner.getSelectedItem();
        }
        if (jahrgang_spinner.getSelectedItem() != null && jahrgang_spinner.getSelectedItemPosition() != 0) {
            year = jahrgang_spinner.getSelectedItemPosition();
        }
        if (!competence_spinner.getSelectedItems().isEmpty()) {
            k = new ArrayList<>();
            for (Kompetenz komp : (List<Kompetenz>) competence_spinner.getSelectedItems()) {
                k.add(komp.getKomId());
            }
        }
        TutoringService service = RetrofitInstance.getInstance().getBuilder().create(TutoringService.class);
        String key = TokenUtil.getInstance().getToken().getKey();
        //Filter without criteria
        if (g == null && year == null && k == null) {
            serviceCall = service.getTutoring(key, null, null, null, null);
        }
        //Selection: Subject and no competence (because no year was selected)
        if (g != null && year == null && k == null) {
            serviceCall = service.getTutoring(key, g.getGegKurzbez(), null, null, null);
        }
        //Selection: Year and subject
        if (g != null && year != null && k == null) {
            serviceCall = service.getTutoring(key, g.getGegKurzbez(), year, null, null);
        }
        //Selection: Year
        if (g == null && year != null && k == null) {
            serviceCall = service.getTutoring(key, null, year, null, null);
        }
        //Selection: Year, subject and competence
        if (g != null && year != null && k != null) {
            serviceCall = service.getTutoring(key, g.getGegKurzbez(), year, k, null);
        }
        serviceCall.enqueue(new Callback<List<Nachhilfe>>() {
            @Override
            public void onResponse(Response<List<Nachhilfe>> response, Retrofit retrofit) {
                searchButton.setEnabled(true);
                search_progressbar.setVisibility(View.GONE);
                if (response.isSuccess()) {
                    List<Nachhilfe> nachhilfe = response.body();
                    Collections.sort(nachhilfe);
                    adapter = new NachhilfeAdapter(getContext(), nachhilfe, false);

                    int index = listview.getFirstVisiblePosition();
                    View v = listview.getChildAt(0);
                    int top = (v == null) ? 0 : v.getTop();
                    listview.setAdapter(adapter);
                    listview.setSelectionFromTop(index, top);

                    if (nachhilfe.isEmpty()) {
                        new AlertDialog.Builder(TutoringFragment.this.getContext())
                                .setTitle(":(")
                                .setMessage("Leider keine Nachhilfe gefunden.")
                                .setPositiveButton("OK", null)
                                .show();
                        return;

                    }
                    listview.post(new Runnable() {
                        @Override
                        public void run() {
                            if(listview.getChildCount() == 0 || listview.getChildAt(0).getTop() == 0){
                                listview.smoothScrollToPositionFromTop(1,0,1000);
                            }
                        }
                    });
                }
            }

            @Override
            public void onFailure(Throwable t) {
                searchButton.setEnabled(true);
                search_progressbar.setVisibility(View.GONE);
                Toast.makeText(TutoringFragment.this.getContext(), "Keine Internetverbindung", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * This class is used to load the competences
     * from the API and sets the competence listener.
     */
    class LoadCompetenceListener implements AdapterView.OnItemSelectedListener {

        private MultiSpinner.MultiSpinnerListener listener = new MultiSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                //do nothing
            }
        };

        /**
         * Sets the competence spinner of a placeholder or all
         * competence topics, who are available.
         */
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if(parent.equals(jahrgang_spinner)){
                competence_spinner.setItems(new ArrayList(), "Jahrgang und Gegenstand auswählen", listener);
                competence_spinner.setEnabled(false);
                setSpinnerToGegenstand();
                return;
            }
            if (jahrgang_spinner.getSelectedItem() == null || subject_spinner.getSelectedItem() == null) {
                competence_spinner.setItems(new ArrayList(), "Jahrgang und Gegenstand auswählen", listener);
                competence_spinner.setEnabled(false);
                return;
            }
            if (subject_spinner.getSelectedItemPosition() == 0 || jahrgang_spinner.getSelectedItemPosition() == 0) {
                competence_spinner.setItems(new ArrayList(), "Jahrgang und Gegenstand auswählen", listener);
                competence_spinner.setEnabled(false);
                return;
            }
            competence_spinner.setEnabled(false);
            competence_spinner.setItems(new ArrayList(), "Kompetenzen werden geladen...", listener);

            Gegenstand g = (Gegenstand) subject_spinner.getSelectedItem();
            Integer year = Integer.parseInt("" + ((String) jahrgang_spinner.getSelectedItem()).charAt(0));
            SubjectService service = RetrofitInstance.getInstance().getBuilder().create(SubjectService.class);
            service.getAvailableCompetence(TokenUtil.getInstance().getToken().getKey(), g.getGegKurzbez(), year)
                    .enqueue(new Callback<List<Kompetenz>>() {
                        @Override
                        public void onResponse(Response<List<Kompetenz>> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                List<Kompetenz> list = response.body();
                                competence_spinner.setItems(list, "Alle", listener);
                                competence_spinner.setEnabled(true);
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            competence_spinner.setItems(new ArrayList(), "Jahrgang und Gegenstand auswählen", listener);
                            Toast.makeText(TutoringFragment.this.getContext(), getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
                        }
                    });
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            //do nothing
        }
    }
}