package at.htlstp.infofox_android.fragments.nachhilfe;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.models.Kompetenz;
import at.htlstp.infofox_android.models.Nachhilfe;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.SubjectUtil;
import tyrantgit.explosionfield.ExplosionField;

/**
 * Shows one tutoring object in detail.
 */
public class TutoringShowFragment extends Fragment {

    private View view;
    private TextView subject_title;
    private View background;
    private View circle;
    private ImageView subject_icon;
    private TextView schueler_text;
    private TextView jahrgang_text;
    private LinearLayout competence_list;
    private TextView comment_text;
    private ScrollView scrollView;

    private Nachhilfe nachhilfe;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nachhilfe = (Nachhilfe)this.getArguments().getSerializable(Constants.TUTORING);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        if(view != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_nachhilfe_show, container, false);
        scrollView = (ScrollView) view.findViewById(R.id.nachhilfe_show_scroll_list);
        subject_title = (TextView) view.findViewById(R.id.nachhilfe_detail_subject);
        circle = view.findViewById(R.id.nachhilfe_detail_circle);
        subject_icon = (ImageView) view.findViewById(R.id.nachhilfe_detail_subject_icon);
        background = view.findViewById(R.id.nachhilfe_detail_background);
        schueler_text = (TextView) view.findViewById(R.id.nachhilfe_detail_schueler);
        jahrgang_text = (TextView) view.findViewById(R.id.nachhilfe_detail_jahrgang);
        competence_list = (LinearLayout) view.findViewById(R.id.nachhilfe_show_competence_list);
        comment_text = (TextView) view.findViewById(R.id.nachhilfe_detail_comment);

        scrollView.setVerticalScrollBarEnabled(false);

        subject_icon.setImageDrawable(getResources().getDrawable(SubjectUtil.getImageResource(nachhilfe.getGegenstand().getGegKurzbez())));
        subject_title.setText(nachhilfe.getGegenstand().getGegLangbez());

        StringBuilder sb = new StringBuilder(nachhilfe.getSchueler().getSsdVorname());
        sb.append(" ");
        sb.append(nachhilfe.getSchueler().getSsdZuname());
        sb.append(" (");
        sb.append(nachhilfe.getSchueler().getKlaBezeichnung().getKlaBezeichung());
        sb.append(")");
        schueler_text.setText(sb.toString());
        jahrgang_text.setText(nachhilfe.getNachJahrgang() + ". Jahrgang");

        for(Kompetenz k : nachhilfe.getKompetenz()){
            View view = inflater.inflate(R.layout.list_item_nachhilfe_competence, null);
            TextView competence = (TextView) view.findViewById(R.id.nachhilfe_competence_list_item);
            competence.setText(k.getKomBezeichnung());
            competence_list.addView(view);
        }

        comment_text.setText(nachhilfe.getNachKommentar());

        //Easter egg :)
        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExplosionField explosionField = ExplosionField.attach2Window(TutoringShowFragment.this.getActivity());
                explosionField.explode(subject_icon);
                explosionField.explode(circle);
                circle.setOnClickListener(null);
            }
        });

        //Scale the logo in
        Animation scaleIn = AnimationUtils.loadAnimation(TutoringShowFragment.this.getContext(), R.anim.scale_fab_in);
        scaleIn.setStartOffset(350);
        circle.startAnimation(scaleIn);
        subject_icon.startAnimation(scaleIn);

        Animation fadeIn = AnimationUtils.loadAnimation(TutoringShowFragment.this.getContext(), R.anim.fade_in);
        fadeIn.setStartOffset(150);
        background.startAnimation(fadeIn);

        return  view;
    }
}
