package at.htlstp.infofox_android.fragments.nachhilfe;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.models.ErrorMessage;
import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Klasse;
import at.htlstp.infofox_android.models.Kompetenz;
import at.htlstp.infofox_android.models.Nachhilfe;
import at.htlstp.infofox_android.services.ClassService;
import at.htlstp.infofox_android.services.SubjectService;
import at.htlstp.infofox_android.services.TutoringService;
import at.htlstp.infofox_android.util.ArrayAdapterUtil;
import at.htlstp.infofox_android.util.ErrorParser;
import at.htlstp.infofox_android.util.MultiSpinner;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * This fragment offers the ability add a new tutoring
 * object in the InfoFox-API. The user can enter a comment
 * the subject and also the class in which they want
 * to offer the tutoring.
 */
public class TutoringOfferFragment extends Fragment implements View.OnClickListener {

    public static final String LOG_TAG = TutoringOfferFragment.class.getSimpleName();
    private View view;
    private ScrollView scrollView;
    private Spinner year_spinner;
    private Spinner subject_spinner;
    private MultiSpinner competence_spinner;
    private Button offerButton;
    private EditText comment;
    private TextView remaining;

    private Klasse klasseFromSchueler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            return view;
        }
        view = inflater.inflate(R.layout.fragment_nachhilfe_anbieten, null);
        scrollView = (ScrollView) view.findViewById(R.id.nachhilfe_anbieten_scrollview);
        year_spinner = (Spinner) view.findViewById(R.id.jahrgang_anbieten_spinner);
        subject_spinner = (Spinner) view.findViewById(R.id.subject_nachhilfe_spinner);
        competence_spinner = (MultiSpinner) view.findViewById(R.id.competence_anbieten_spinner);
        offerButton = (Button) view.findViewById(R.id.offerButton);
        comment = (EditText) view.findViewById(R.id.nachhilfe_anbieten_comment);
        remaining = (TextView) view.findViewById(R.id.remaining_chars);

        remaining.setText("Kontaktinformation (min. 20 Zeichen)");
        competence_spinner.setEnabled(false);

        //Load the subject for the years
        year_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setSpinnerToGegenstand();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        subject_spinner.setOnItemSelectedListener(new LoadCompetenceListener());
        offerButton.setOnClickListener(this);

        //Show information about the current status of the comment field
        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String input = comment.getText().toString().trim();
                int remain = 20 - input.length();
                if (remain == 20) {
                    remaining.setText("Kontaktinformation (min. 20 Zeichen)");
                } else if (remain > 0 && input.length() <= 250) {
                    remaining.setText("Kontaktinformation (noch " + remain + " Zeichen)");
                } else if (input.length() > 250) {
                    remaining.setText("Kontaktinformation (über max. Zeichen)");
                } else {
                    remaining.setText("Kontaktinformation " + getEmijoByUnicode(0x2714));
                }
            }
        });

        //Set the spinner to current class
        setSpinnerToValidClasses();

        scrollView.setVerticalScrollBarEnabled(false);
        return view;
    }

    /**
     * Loads all classes in which he is able to offer
     * tutoring, which means his class and below.
     */
    private void setSpinnerToValidClasses() {
        ClassService service = RetrofitInstance.getInstance().getBuilder().create(ClassService.class);
        service.getClassFromStudent(TokenUtil.getInstance().getToken().getKey()).enqueue(new Callback<Klasse>() {
            @Override
            public void onResponse(Response<Klasse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    klasseFromSchueler = response.body();
                    Integer klasse = Integer.parseInt(klasseFromSchueler.getKlaBezeichung().substring(0, 1));
                    List<String> classes = new ArrayList<>();
                    for (int i = 1; i <= klasse; i++) {
                        classes.add(i + ". Jahrgang");
                    }
                    year_spinner.setAdapter(ArrayAdapterUtil.createAdapter(TutoringOfferFragment.this.getContext(), classes));
                    year_spinner.setSelection(klasse - 1);
                    setSpinnerToGegenstand();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(TutoringOfferFragment.this.getActivity(), getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Loads the subjects for this year.
     */
    private void setSpinnerToGegenstand() {

        Integer year = year_spinner.getSelectedItemPosition() + 1;
        subject_spinner.setEnabled(false);
        List<Gegenstand> fakeList = new ArrayList<>();
        fakeList.add(new Gegenstand("Gegenstände werden geladen..."));
        subject_spinner.setAdapter(ArrayAdapterUtil.createAdapter(TutoringOfferFragment.this.getContext(), fakeList));
        SubjectService service = RetrofitInstance.getInstance().getBuilder().create(SubjectService.class);
        service.getSubjectsByYear(TokenUtil.getInstance().getToken().getKey(), year).enqueue(new Callback<List<Gegenstand>>() {
            @Override
            public void onResponse(Response<List<Gegenstand>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    List<Gegenstand> l = response.body();
                    l.add(0, new Gegenstand("Keine Auswahl"));
                    subject_spinner.setAdapter(ArrayAdapterUtil.createAdapter(TutoringOfferFragment.this.getContext(), l));
                    subject_spinner.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(TutoringOfferFragment.this.getActivity(), getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Tries to save the tutoring object. But first checks if all the
     * input was valid.
     */
    @Override
    public void onClick(View v) {
        String error = null, title = null;
        if (subject_spinner.getAdapter() == null || subject_spinner.getAdapter().getCount() < 1 || subject_spinner.getSelectedItemPosition() == 0) {
            title = "Keine Auswahl";
            error = "Bitte wähle einen Gegenstand aus.";
        } else if (competence_spinner.getItems() == null || competence_spinner.getItems().size() == 0) {
            title = "Keine Auswahl";
            error = "Bitte wähle mindestens einen Kompetenzbereich aus.";
        } else if (comment.getText().toString().trim().length() < 20) {
            title = ":(";
            error = "Bitte gib einen kurzen Kommentar (min. 20 Zeichen) ab.";
        } else if (comment.getText().toString().trim().length() > 250) {
            title = ":(";
            error = "Bitte gib einen kürzeren Kommentar (max 250 Zeichen) ab.";
        }
        if (error != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(title)
                    .setMessage(error)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
            return;
        }

        final ProgressDialog bar = new ProgressDialog(this.getContext());
        bar.setIndeterminate(true);
        bar.setCancelable(false);
        bar.setMessage("Nachhilfe wird gespeichert...");
        bar.show();

        Nachhilfe h = new Nachhilfe();
        List<Integer> ids = new ArrayList<>();
        for (Kompetenz k : (List<Kompetenz>) competence_spinner.getSelectedItems()) {
            ids.add(k.getKomId());
        }
        h.setKomIds(ids);
        h.setSubject(((Gegenstand) subject_spinner.getSelectedItem()).getGegKurzbez());
        h.setNachKommentar(comment.getText().toString().trim());
        h.setNachJahrgang(year_spinner.getSelectedItemPosition() + 1);

        TutoringService service = RetrofitInstance.getInstance().getBuilder().create(TutoringService.class);
        service.insertTutoring(TokenUtil.getInstance().getToken().getKey(), h).enqueue(new Callback<Nachhilfe>() {
            @Override
            public void onResponse(Response<Nachhilfe> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Yeahhhh!")
                            .setMessage("Nachhilfe erfolgreich gespeichert. Danke für deine Hilfe :)")
                            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    TutoringRootFragment.getManager().popBackStack();
                                }
                            })
                            .show();
                } else {
                    ErrorMessage mes = ErrorParser.parseError(response);
                    if (mes.getDesc().startsWith("Tutoring is already offered in")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle("Fehler")
                                .setMessage("Du bietest bereits für diesen Jahrgang und Gegenstand Nachhilfe an.")
                                .setPositiveButton(android.R.string.ok, null)
                                .show();
                    }
                }
                bar.dismiss();
            }

            @Override
            public void onFailure(Throwable t) {
                bar.dismiss();
                new AlertDialog.Builder(TutoringOfferFragment.this.getContext())
                        .setTitle(":(")
                        .setMessage("Konnte keine Verbindung zum Server herstellen.")
                        .setPositiveButton(android.R.string.ok, null)
                        .show();
                return;
            }
        });

    }

    /**
     * Sets the competences for this subject.
     */
    class LoadCompetenceListener implements AdapterView.OnItemSelectedListener {

        private MultiSpinner.MultiSpinnerListener listener = new MultiSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {
                //do nothing
            }
        };

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (year_spinner.getSelectedItem() != null && subject_spinner.getSelectedItem() != null) {
                if (subject_spinner.getSelectedItemPosition() == 0) {
                    competence_spinner.setItems(new ArrayList(), "Jahrgang und Gegenstand auswählen", listener);
                    competence_spinner.setEnabled(false);
                    return;
                }
                competence_spinner.setEnabled(false);
                competence_spinner.setItems(new ArrayList(), "Kompetenzen werden geladen...", listener);

                Gegenstand g = (Gegenstand) subject_spinner.getSelectedItem();
                Integer year = Integer.parseInt("" + ((String) year_spinner.getSelectedItem()).charAt(0));
                SubjectService service = RetrofitInstance.getInstance().getBuilder().create(SubjectService.class);
                service.getAvailableCompetence(TokenUtil.getInstance().getToken().getKey(), g.getGegKurzbez(), year)
                        .enqueue(new Callback<List<Kompetenz>>() {
                            @Override
                            public void onResponse(Response<List<Kompetenz>> response, Retrofit retrofit) {
                                if (response.isSuccess()) {
                                    List<Kompetenz> list = response.body();
                                    competence_spinner.setItems(list, "Alle", listener);
                                    competence_spinner.setEnabled(true);
                                }
                            }

                            @Override
                            public void onFailure(Throwable t) {
                                competence_spinner.setItems(new ArrayList(), "Jahrgang und Gegenstand auswählen", listener);
                                Toast.makeText(TutoringOfferFragment.this.getContext(), "Keine Internetverbindung", Toast.LENGTH_LONG).show();
                            }
                        });

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    /**
     * Returns the emoji character for the
     * given unicode value.
     */
    public String getEmijoByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

}
