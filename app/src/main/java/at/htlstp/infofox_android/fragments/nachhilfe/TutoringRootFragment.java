package at.htlstp.infofox_android.fragments.nachhilfe;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import at.htlstp.infofox_android.MainTabActivity;
import at.htlstp.infofox_android.R;

/**
 * For Documentation look into VmmRootFragment, they are basically the same.
 */
public class TutoringRootFragment extends Fragment {

    public static final String LOG_TAG = TutoringRootFragment.class.getSimpleName();

    private Bundle bundle;
    private View view;

    private static FragmentManager manager;

    public TutoringRootFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        bundle = this.getArguments();
        TutoringRootFragment.manager = getChildFragmentManager();
        int count = manager.getBackStackEntryCount();
        for(int i = 0; i < count; i++){
            manager.popBackStackImmediate();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.fragment_nachhilfe_root, container, false);
        }
        if(savedInstanceState == null) {
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            TutoringFragment nf = new TutoringFragment();
            nf.setArguments(bundle);
            transaction.replace(R.id.nachhilfe_root, nf);
            transaction.commit();
        }
        return view;
    }

    public void onBackPressed(){
        if(manager.getBackStackEntryCount() == 0){
            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addCategory(Intent.CATEGORY_HOME);
            //pretty dirty, but it gets the work done
            MainTabActivity.getMaincontext().startActivity(i);
        } else {
            manager.popBackStack();
        }
    }

    public static FragmentManager getManager() {
        return manager;
    }

}
