package at.htlstp.infofox_android.fragments.nachhilfe;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.models.ErrorMessage;
import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Kompetenz;
import at.htlstp.infofox_android.models.Nachhilfe;
import at.htlstp.infofox_android.services.SubjectService;
import at.htlstp.infofox_android.services.TutoringService;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.ErrorParser;
import at.htlstp.infofox_android.util.MultiSpinner;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.SubjectUtil;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import tyrantgit.explosionfield.ExplosionField;

/**
 * Offers the possibility to edit a already offered tutoring object
 * and also adds the ability to delete tutoring object.
 */
public class TutoringEditDetailFragment extends Fragment {

    public static final String LOG_TAG = TutoringEditDetailFragment.class.getSimpleName();

    private View view;
    private View circle;
    private ImageView subject_icon;
    private MultiSpinner competence_spinner;
    private EditText comment_text;
    private TextView remaining;

    private Nachhilfe nachhilfe;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        nachhilfe = (Nachhilfe)this.getArguments().getSerializable(Constants.TUTORING);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            return view;
        }
        view = inflater.inflate(R.layout.fragment_nachhilfe_edit_detail, null);

        ScrollView scrollView = (ScrollView) view.findViewById(R.id.nachhilfe_edit_detail_scrollView);
        View background = (View) view.findViewById(R.id.nachhilfe_detail_edit_background);
        circle = (View) view.findViewById(R.id.nachhilfe_detail_edit_circle);
        TextView subject_title = (TextView) view.findViewById(R.id.nachhilfe_detail_edit_subject);
        subject_icon = (ImageView) view.findViewById(R.id.nachhilfe_detail_edit_subject_icon);
        TextView year_text = (TextView) view.findViewById(R.id.nachhilfe_detail_edit_jahrgang);
        competence_spinner = (MultiSpinner) view.findViewById(R.id.nachhilfe_detail_edit_competence);
        comment_text = (EditText) view.findViewById(R.id.nachhilfe_detail_edit_comment);
        remaining = (TextView) view.findViewById(R.id.nachhilfe_detail_edit_comment_remaining);
        Button saveButton = (Button) view.findViewById(R.id.saveButton);
        Button delButton = (Button) view.findViewById(R.id.delButton);

        scrollView.setVerticalScrollBarEnabled(false);

        //Load all competences from the API
        loadCompetences();

        subject_icon.setImageDrawable(getResources().getDrawable(SubjectUtil.getImageResource(nachhilfe.getGegenstand().getGegKurzbez())));
        subject_title.setText(nachhilfe.getGegenstand().getGegLangbez());
        year_text.setText(nachhilfe.getNachJahrgang() + ". Jahrgang");

        //Show information about the current status of the comment field
        comment_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String input = comment_text.getText().toString().trim();
                int remain = 20 - input.length();
                if (remain == 20) {
                    remaining.setText("Kontaktinformation (min. 20 Zeichen)");
                } else if (remain > 0 && input.length() <= 250) {
                    remaining.setText("Kontaktinformation (noch " + remain + " Zeichen)");
                } else if (input.length() > 250) {
                    remaining.setText("Kontaktinformation (über max. Zeichen)");
                } else {
                    remaining.setText("Kontaktinformation " + getEmijoByUnicode(0x2714));
                }
            }
        });

        comment_text.setText(nachhilfe.getNachKommentar());

        //Save the changes
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTutoring();
            }
        });

        //Delete the tutoring object, also show a confirmation dialog
        delButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Entfernen")
                        .setMessage("Möchtest du die Nachhilfe wirklich entfernen?")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteTutoring();
                            }
                        })
                        .setNegativeButton("Abbrechen", null)
                        .show();
            }
        });

        //Easter egg :)
        circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExplosionField explosionField = ExplosionField.attach2Window(TutoringEditDetailFragment.this.getActivity());
                explosionField.explode(subject_icon);
                explosionField.explode(circle);

                circle.setOnClickListener(null);
            }
        });

        //Scale the logo in and out
        Animation scaleIn = AnimationUtils.loadAnimation(TutoringEditDetailFragment.this.getContext(), R.anim.scale_fab_in);
        scaleIn.setStartOffset(350);
        circle.startAnimation(scaleIn);
        subject_icon.startAnimation(scaleIn);

        Animation fadeIn = AnimationUtils.loadAnimation(TutoringEditDetailFragment.this.getContext(), R.anim.fade_in);
        fadeIn.setStartOffset(150);
        background.startAnimation(fadeIn);

        return view;
    }

    /**
     * Load all competences, which belongs to the turoing object.
     */
    public void loadCompetences() {
        final MultiSpinner.MultiSpinnerListener listener = new MultiSpinner.MultiSpinnerListener(){

            @Override
            public void onItemsSelected(boolean[] selected) {
                //do nothing
            }
        };
        competence_spinner.setEnabled(false);
        competence_spinner.setItems(new ArrayList(), "Kompetenzen werden geladen...", listener);

        Gegenstand g = nachhilfe.getGegenstand();
        Integer year = nachhilfe.getNachJahrgang();
        SubjectService service = RetrofitInstance.getInstance().getBuilder().create(SubjectService.class);
        service.getAvailableCompetence(TokenUtil.getInstance().getToken().getKey(), g.getGegKurzbez(), year)
                .enqueue(new Callback<List<Kompetenz>>() {
                    @Override
                    public void onResponse(Response<List<Kompetenz>> response, Retrofit retrofit) {
                        if (response.isSuccess()) {
                            List<Kompetenz> list = response.body();
                            boolean selected[] = new boolean[list.size()];
                            for (int i = 0; i < list.size(); i++) {
                                Kompetenz k = list.get(i);
                                boolean checked = false;
                                for (Kompetenz nach_k : nachhilfe.getKompetenz()) {
                                    if (k.equals(nach_k)) {
                                        checked = true;
                                        break;
                                    }
                                }
                                selected[i] = checked;
                            }
                            competence_spinner.setItems(list, "Alle", listener);
                            competence_spinner.setSelected(selected);
                            competence_spinner.setEnabled(true);
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Toast.makeText(TutoringEditDetailFragment.this.getContext(), "Keine Internetverbindung", Toast.LENGTH_LONG).show();
                    }
                });

    }

    /**
     * Tries to save the object in the InfoFox-API.
     * But first checks if the input is valid.
     */
    public void updateTutoring(){
        String title = null;
        String error = null;
        if (comment_text.getText().toString().trim().length() < 20) {
            title = ":(";
            error = "Bitte gib einen kurzen Kommentar (min. 20 Zeichen) ab.";
        } else if (comment_text.getText().toString().trim().length() > 250) {
            title = ":(";
            error = "Bitte gib einen kürzeren Kommentar (max 250 Zeichen) ab.";
        }
        if(error != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(title)
                    .setMessage(error)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
            return;
        }

        final ProgressDialog bar = new ProgressDialog(this.getContext());
        bar.setIndeterminate(true);
        bar.setCancelable(false);
        bar.setMessage("Nachhilfe wird gespeichert...");
        bar.show();

        List<Integer> ids = new ArrayList<>();
        for(Kompetenz k : (List<Kompetenz>)competence_spinner.getSelectedItems()){
            ids.add(k.getKomId());
        }
        nachhilfe.setKomIds(ids);
        nachhilfe.setSubject(nachhilfe.getGegenstand().getGegKurzbez());
        nachhilfe.setNachKommentar(comment_text.getText().toString());

        TutoringService service = RetrofitInstance.getInstance().getBuilder().create(TutoringService.class);
        service.updateTutoring(TokenUtil.getInstance().getToken().getKey(), nachhilfe.getNachId(), nachhilfe).enqueue(new Callback<Nachhilfe>() {
            @Override
            public void onResponse(Response<Nachhilfe> response, Retrofit retrofit) {
                bar.dismiss();
                if(!response.isSuccess()){
                    ErrorMessage mes = ErrorParser.parseError(response);
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle(":(")
                            .setMessage("Nachhilfe konnte nicht gespeichert werden.")
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                } else {
                    Toast.makeText(TutoringEditDetailFragment.this.getContext(), "Nachhilfe gespeichert", Toast.LENGTH_SHORT).show();
                    TutoringRootFragment.getManager().popBackStack();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                bar.dismiss();
                Toast.makeText(TutoringEditDetailFragment.this.getContext(), R.string.network_error, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Delete the tutoring object.
     */
    public void deleteTutoring(){
        final ProgressDialog bar = new ProgressDialog(this.getContext());
        bar.setIndeterminate(true);
        bar.setCancelable(false);
        bar.setMessage("Nachhilfe wird entfernt...");
        bar.show();

        TutoringService service = RetrofitInstance.getInstance().getBuilder().create(TutoringService.class);
        service.delTutoring(TokenUtil.getInstance().getToken().getKey(),nachhilfe.getNachId()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Response<Void> response, Retrofit retrofit) {
                bar.dismiss();
                if(response.isSuccess()){
                    FragmentManager manager = TutoringRootFragment.getManager();
                    manager.beginTransaction()
                            .remove(TutoringEditDetailFragment.this)
                            .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                            .commit();
                    manager.popBackStack();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getContext(),R.string.network_error,Toast.LENGTH_LONG).show();
                bar.dismiss();
            }
        });
    }

    /**
     * Returns the emoji character for the
     * given unicode value.
     */
    public String getEmijoByUnicode(int unicode){
        return new String(Character.toChars(unicode));
    }

}
