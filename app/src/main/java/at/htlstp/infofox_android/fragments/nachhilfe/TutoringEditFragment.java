package at.htlstp.infofox_android.fragments.nachhilfe;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.adapter.NachhilfeAdapter;
import at.htlstp.infofox_android.models.ErrorMessage;
import at.htlstp.infofox_android.models.Nachhilfe;
import at.htlstp.infofox_android.services.TutoringService;
import at.htlstp.infofox_android.util.ErrorParser;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Shows a listview of all available tutoring
 * objects, which belong to the logged in user.
 */
public class TutoringEditFragment extends Fragment {

    public static final String LOG_TAG = TutoringEditFragment.class.getSimpleName();

    private View view;
    private ListView myTutoringList;
    private ProgressBar bar;

    private NachhilfeAdapter adapter;

    public TutoringEditFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view != null) {
            return view;
        }
        view = inflater.inflate(R.layout.fragment_nachhilfe_edit, null);
        myTutoringList = (ListView) view.findViewById(R.id.tutoring_my_list);
        bar = (ProgressBar) view.findViewById(R.id.nachhilfe_my_search);

        myTutoringList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

                if (scrollState == SCROLL_STATE_TOUCH_SCROLL || scrollState == SCROLL_STATE_FLING) {
                    adapter.setIsMoving(true);
                } else {
                    adapter.setIsMoving(false);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //Do nothing
            }

        });

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        myTutoringList.setAdapter(null);
        setMyTutoring();
    }

    /**
     * Loads all tutoring objects from the
     * InfoFox-API, which belongs to the user who
     * is logged in.
     */
    public void setMyTutoring() {
        bar.setVisibility(View.VISIBLE);
        TutoringService service = RetrofitInstance.getInstance().getBuilder().create(TutoringService.class);
        service.getTutoring(TokenUtil.getInstance().getToken().getKey(), null, null, null, true).enqueue(new Callback<List<Nachhilfe>>() {
            @Override
            public void onResponse(Response<List<Nachhilfe>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    List<Nachhilfe> list = response.body();

                    if(list.isEmpty()) {

                        if(TutoringEditFragment.this.getContext() == null){
                            return;
                        }
                        /**
                         * Redirect the user to add the tutoring
                         * or not. As he likes.
                         */
                        new AlertDialog.Builder(TutoringEditFragment.this.getContext())
                                .setTitle(":(")
                                .setMessage("Leider keine Angebote gefunden.\n\nMöchtest du Nachhilfe anbieten?")
                                .setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        FragmentTransaction transaction = TutoringRootFragment.getManager().beginTransaction();
                                        transaction.remove(TutoringEditFragment.this);
                                        TutoringOfferFragment nf = new TutoringOfferFragment();
                                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                        transaction.replace(R.id.nachhilfe_root, nf);
                                        transaction.addToBackStack(null);
                                        transaction.commit();
                                    }
                                })
                                .setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        TutoringRootFragment.getManager().popBackStack();
                                    }
                                })
                                .show();
                    }

                    adapter = new NachhilfeAdapter(TutoringEditFragment.this.getContext(), list, true);
                    myTutoringList.setAdapter(adapter);
                }
                bar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(TutoringEditFragment.this.getActivity(), getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
                bar.setVisibility(View.GONE);
            }
        });
    }

}