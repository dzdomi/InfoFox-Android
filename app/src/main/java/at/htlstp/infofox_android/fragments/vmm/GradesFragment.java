package at.htlstp.infofox_android.fragments.vmm;


import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import at.htlstp.infofox_android.MainTabActivity;
import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.adapter.GradesAdapter;
import at.htlstp.infofox_android.database.InfoFoxSQLHelper;
import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Laufendenote;
import at.htlstp.infofox_android.services.GradeService;
import at.htlstp.infofox_android.util.AnimationUtil;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.SubjectUtil;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;
import xyz.hanks.library.SmallBang;

/**
 * Displays a listview with a header and all grades of the subjects.
 * In the header the subject is displayed. At the bottom of the page
 * the current grade in this subject is displayed.
 */
public class GradesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener, View.OnClickListener, AdapterView.OnItemLongClickListener {

    public static final String LOG_TAG = GradesFragment.class.getSimpleName();

    private View view;
    private View header;

    private Gegenstand subject;
    private ListView gradeList;
    private SwipeRefreshLayout swipeLayout;
    private LineChart gradeChart;
    private ImageView grade_icon;
    private TextView subjectTitle;
    private TextView gradeSummary;
    private ImageView gradeSummaryArrow;
    private RelativeLayout graphic_layout;
    private View circle;
    private View background;

    public GradesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        subject = (Gegenstand)this.getArguments().getSerializable(Constants.GRADES);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(view != null && header != null){
            return view;
        }
        view = inflater.inflate(R.layout.fragment_grades, container, false);
        header = inflater.inflate(R.layout.listview_header_grades, null);

        gradeList = (ListView) view.findViewById(R.id.grades_listview);
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.grades_swipe_refresh);
        gradeSummary = (TextView) view.findViewById(R.id.grade_summary);
        gradeSummaryArrow = (ImageView) view.findViewById(R.id.grade_summary_arrow);

        subjectTitle = (TextView)header.findViewById(R.id.grades_detail_subject);
        gradeChart = (LineChart) header.findViewById(R.id.grade_chart);
        grade_icon = (ImageView) header.findViewById(R.id.grades_detail_subject_icon);
        graphic_layout = (RelativeLayout) header.findViewById(R.id.grade_chart_layout);
        circle = header.findViewById(R.id.grades_detail_circle);
        background = header.findViewById(R.id.grades_detail_background);

        grade_icon.setImageDrawable(getResources().getDrawable(SubjectUtil.getImageResource(subject.getGegKurzbez())));
        gradeList.addHeaderView(header);

        grade_icon.setOnClickListener(this);
        header.setOnClickListener(null);
        gradeList.setOnItemClickListener(this);
        gradeList.setOnItemLongClickListener(this);
        swipeLayout.setOnRefreshListener(this);

        subjectTitle.setText(subject.getGegLangbez() + " - " + subject.getTeacher());

        //Sort grades after the date
        List<Laufendenote> grades = InfoFoxSQLHelper.getInstance().getAllGradesFromSubject(subject);
        Collections.sort(grades);
        gradeList.setAdapter(new GradesAdapter(getActivity(), grades));
        //Set up our chart with the data
        setupChart(gradeChart, getGradeData());

        /**
         * Set the oriantation and the color of the
         * arrow at the buttom of the site.
         */
        if(grades.size() >= 2){
            int result = Double.compare(grades.get(1).getLntRealBewertung(), grades.get(0).getLntRealBewertung());
            if(result != 0) {
                if (result > 0) {
                    gradeSummaryArrow.setImageResource(R.drawable.increase);
                } else {
                    gradeSummaryArrow.setImageResource(R.drawable.decrease);
                }
            } else {
                gradeSummaryArrow.setVisibility(View.GONE);
            }
        } else {
            gradeSummaryArrow.setVisibility(View.GONE);
        }

        gradeSummary.setText(getGesamt(grades.get(0).getLntRealBewertung()));

        //Animations for the arrow
        AnimationSet set = new AnimationSet(true);
        Animation scale = AnimationUtils.loadAnimation(this.getActivity(), R.anim.scaletofullsize);
        scale.setDuration(800);
        set.addAnimation(scale);
        Animation rotate = AnimationUtils.loadAnimation(this.getActivity(), R.anim.rotate);
        rotate.setDuration(800);
        set.addAnimation(rotate);
        gradeSummaryArrow.startAnimation(set);

        Animation scaleIn = AnimationUtils.loadAnimation(GradesFragment.this.getContext(), R.anim.scale_fab_in);
        scaleIn.setStartOffset(350);
        circle.startAnimation(scaleIn);
        grade_icon.startAnimation(scaleIn);

        Animation fadeIn = AnimationUtils.loadAnimation(GradesFragment.this.getContext(), R.anim.fade_in);
        fadeIn.setStartOffset(150);
        background.startAnimation(fadeIn);

        return view;
    }

    /**
     * Loads the new data from the API and stores
     * it in the database.
     */
    @Override
    public void onRefresh() {
        GradeService service = RetrofitInstance.getInstance().getBuilder().create(GradeService.class);
        service.getGradesFromSubject(TokenUtil.getInstance().getToken().getKey(), subject.getGegKurzbez(), subject.getTeacher()).enqueue(new Callback<List<Laufendenote>>() {
            @Override
            public void onResponse(Response<List<Laufendenote>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    List<Laufendenote> list = response.body();
                    InfoFoxSQLHelper.getInstance().deleteAndInsertGradesFromSubject(subject, list);
                    List<Laufendenote> grades = InfoFoxSQLHelper.getInstance().getAllGradesFromSubject(subject);
                    Collections.sort(grades);
                    gradeList.setAdapter(new GradesAdapter(getActivity(), grades));
                    setupChart(gradeChart,getGradeData());
                }
                swipeLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(GradesFragment.this.getActivity(), getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
                swipeLayout.setRefreshing(false);
            }
        });
    }

    /**
     * On item click show the chartview, if it is not already expanded.
     * Also highlight the point in the graph, or unhighlight it.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Highlight highlight = gradeChart.getHighlighted() != null ? gradeChart.getHighlighted()[0] : null;
        //Calculate current right position, because view is reversed and first item is the header
        position = gradeList.getAdapter().getCount() - position - 1;
        //If item is already highlighted and user selected the same symbol, then remove highlighting
        if (highlight != null) {
            if (position == highlight.getXIndex()) {
                gradeChart.highlightValue(null);
                return;
            }
        }
        if (graphic_layout.getVisibility() == View.GONE) {
            onClick(null);
        }
        gradeChart.highlightValue(position, 0);
    }

    /**
     * Variables for the small easter egg.
     * Just show it every 15th time.
     */
    private int counter = 0;
    private int random = new Random().nextInt(15) + 1;
    private boolean boom = false;
    private SmallBang bang;
    @Override
    public void onClick(View v) {
        if(!boom && counter == random){
            counter = 0;
            if(bang == null){
                bang = SmallBang.attach2Window(this.getActivity());
                bang.setColors(new int[]{getResources().getColor(R.color.button_color_presses), getResources().getColor(R.color.explosion)});
            }
            random = new Random().nextInt(15) + 1;
            boom = true;
        } else if(!boom) {
            counter++;
        }

        /**
         * Expands or collapses the view.
         * And also sets the bang for the easter egg.
         */
        if(graphic_layout.getVisibility() == View.GONE){
            grade_icon.setImageDrawable(getResources().getDrawable(R.drawable.arrow_blue));
            if(boom){
                bang.bang(grade_icon);
                boom = false;
            }
            AnimationUtil.expand(graphic_layout, 300);
            gradeChart.animateX(1500);
        } else {
            grade_icon.setImageDrawable(getResources().getDrawable(SubjectUtil.getImageResource(subject.getGegKurzbez())));
            AnimationUtil.collapse(graphic_layout, 300);
        }

    }

    /**
     * Sets up the graphical parts for the chart. Most of the
     * documentation can be found under the following link:
     * https://github.com/PhilJay/MPAndroidChart/wiki
     */
    private void setupChart(LineChart chart, LineData data) {

        // no description text
        chart.setDescription("");

        //No touch enabled
        chart.setTouchEnabled(false);
        chart.setHighlightPerTapEnabled(true);
        chart.setPinchZoom(true);

        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setDrawAxisLine(true);

        chart.setData(data);

        // get the legend (only possible after setting data)
        Legend l = chart.getLegend();
        l.setEnabled(false);

        chart.getAxisRight().setEnabled(false);

        chart.getAxisLeft().setEnabled(true);
        chart.getAxisLeft().setInverted(true);
        chart.getAxisLeft().setAxisMaxValue(6.0f);
        chart.getAxisLeft().setAxisMinValue(0.0f);
    }

    /**
     * Creates a dataset for the current grades.
     */
    private LineData getGradeData() {

        List<Entry> grades = new ArrayList<>();
        List<String> xVals = new ArrayList<>();

        //Header counts as an item, so therefore start at size - 1
        for(int i = gradeList.getAdapter().getCount() - 1, j = 0; i > 0; i--, j++){
            Laufendenote note = (Laufendenote)gradeList.getAdapter().getItem(i);
            grades.add(new Entry(note.getLntRealBewertung().floatValue(),j));
        }
        //Add the time to the xValues
        for(int i = gradeList.getAdapter().getCount() - 1; i > 0; i--){
            Laufendenote note = (Laufendenote)gradeList.getAdapter().getItem(i);
            Calendar cal = Calendar.getInstance();
            cal.setTime(note.getLntTimestamp());
            //Month + 1 because Months are indexed by 0, WTF Java Devs what were you thinking
            xVals.add(cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH) + 1) + ".");
        }

        LineDataSet set = setupLineDataSet(grades, "Dataset 1");

        ArrayList<LineDataSet> datasets = new ArrayList<>();
        datasets.add(set); // add the datasets

        return new LineData(xVals, datasets);
    }

    /**
     * Shows a info message of the grade if the user taps long
     * enough on the grade.
     */
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Laufendenote note = (Laufendenote)gradeList.getAdapter().getItem(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle("Bemerkung")
                .setMessage(note.getLntBemerkung())
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        return true;
    }

    /**
     * Sets up a list of entries for the given dataset,
     * mostly formatting and coloring.
     */
    private LineDataSet setupLineDataSet(List<Entry> entries, String subject_short){
        final LineDataSet set = new LineDataSet(entries, subject_short);
        set.setLineWidth(3f);
        set.setValueTextSize(10f);
        set.setCircleSize(5.5f);
        set.setColor(Color.BLACK);
        set.setCircleColor(MainTabActivity.getMaincontext().getResources().getColor(R.color.grundfarbe));
        set.setCircleColorHole(MainTabActivity.getMaincontext().getResources().getColor(R.color.grundfarbe));
        set.setHighLightColor(Color.BLUE);
        set.setHighlightLineWidth(1f);
        set.setDrawValues(true);
        /**
         * Sets the value of a string if it exceeds the entries over 15.
         * It only shows every third and the last value.
         */
        set.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                if(set.getEntryCount() > 15){
                    if(entry.getXIndex() % 3 == 0 || entry.getXIndex() == set.getEntryCount() - 1){
                        return String.format("%.1f", value);
                    } else {
                        return "";
                    }
                }
                return String.format("%.1f", value);
            }
        });
        return set;
    }

    /**
     * Calculates the string value of the given rating.
     */
    private String getGesamt(Double bewertung){

        if(bewertung < 1.5){
            return "1";
        } else if(bewertung >= 1.5 && bewertung < 2.5){
            return "2";
        } else if(bewertung >= 2.5 && bewertung < 3.5){
            return "3";
        } else if(bewertung >= 3.5 && bewertung < 4.5){
            return "4";
        } else {
            return "5";
        }
    }
}
