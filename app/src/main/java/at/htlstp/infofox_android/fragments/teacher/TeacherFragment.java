package at.htlstp.infofox_android.fragments.teacher;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import at.htlstp.infofox_android.MainTabActivity;
import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.models.ErrorMessage;
import at.htlstp.infofox_android.models.Klasse;
import at.htlstp.infofox_android.models.Personal;
import at.htlstp.infofox_android.services.ClassService;
import at.htlstp.infofox_android.services.TeacherService;
import at.htlstp.infofox_android.util.ArrayAdapterUtil;
import at.htlstp.infofox_android.util.ErrorParser;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * This fragment holds the information about different teacher information.
 * It is used in the Skipactivity and also in the normal MainTabActivity.
 */
public class TeacherFragment extends Fragment {

    private static String LOG_TAG = TeacherFragment.class.getSimpleName();

    private View view;
    private Spinner classSpinner;
    private Spinner teacherSpinner;
    private TextView hours;
    private TextView contact_tel;
    private TextView contact_email;
    private TextView av_tel;
    private TextView av_email;

    private Klasse k;

    private Context context;

    public TeacherFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null) {
            return view;
        }
        view = inflater.inflate(R.layout.fragment_teacher, container, false);
        classSpinner = (Spinner) view.findViewById(R.id.class_spinner);
        teacherSpinner = (Spinner) view.findViewById(R.id.teacher_spinner);
        hours = (TextView) view.findViewById(R.id.teacher_hours);
        contact_email = (TextView) view.findViewById(R.id.teacher_contact_email);
        contact_tel = (TextView) view.findViewById(R.id.teacher_contact_tel);
        av_email = (TextView) view.findViewById(R.id.teacher_av_email);
        av_tel = (TextView) view.findViewById(R.id.teacher_av_tel);

        /**
         * Get all the classes and the information about the av.
         * This can be called on the beginning, as this data is mostly static.
         */
        getClasses();
        getAV();

        /**If one item is selected on the teacherspinner, we need to load the
         * hours and contact information about this teacher.
         */
        teacherSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setHours();
                setContact();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /**
         * If a class was selected, we have to load the information from the
         * InfoFox-API.
         */
        classSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getTeachers();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    /**
     * Return back to the home screen.
     */
    public void onBackPressed() {
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addCategory(Intent.CATEGORY_HOME);
        //pretty dirty, but it gets the work done
        MainTabActivity.getMaincontext().startActivity(i);

    }

    /**
     * This method is important for the orientation changes, otherwise
     * the context is not the correct wrong, or does not exist anymore.
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    /**
     * Loads all classes from the InfoFox-API.
     */
    private void getClasses() {

        final ClassService cs = RetrofitInstance.getInstance().getBuilder().create(ClassService.class);
        cs.getClasses().enqueue(new Callback<List<Klasse>>() {
            @Override
            public void onResponse(Response<List<Klasse>> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    List<Klasse> l = response.body();
                    Collections.sort(l);
                    l.add(0, new Klasse("Keine Auswahl"));
                    ArrayAdapter<Klasse> adapter = ArrayAdapterUtil.createAdapter(context, l);
                    classSpinner.setAdapter(adapter);
                    //After loading the classes, check for the own class and set it to the right one
                    setOwnClass();
                }
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

    /**
     * If the user is logged in. The current class will be set to
     * the correct one.
     */
    public void setOwnClass(){
        String key = null;
        try{
            key = TokenUtil.getInstance().getToken().getKey();
        } catch (IllegalStateException e){
            //Not logged in, don't set class
            return;
        }
        ClassService cs = RetrofitInstance.getInstance().getBuilder().create(ClassService.class);
        cs.getClassFromStudent(key).enqueue(new Callback<Klasse>() {
            @Override
            public void onResponse(Response<Klasse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    k = response.body();
                    int pos = ((ArrayAdapter)classSpinner.getAdapter()).getPosition(k);
                    classSpinner.setSelection(pos);
                    getTeachers();
                }
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

    /**
     * Loads all teachers from the class, or if it is not selected,
     * all teachers will be retrieved from the server.
     */
    private void getTeachers() {
        teacherSpinner.setEnabled(false);

        //This list is needed to show something to the user
        List<String> fakeList = new ArrayList<>();
        fakeList.add("Lehrer werden geladen...");
        teacherSpinner.setAdapter(ArrayAdapterUtil.createAdapter(context,fakeList));

        Klasse klasse = (Klasse) classSpinner.getSelectedItem();

        TeacherService ts = RetrofitInstance.getInstance().getBuilder().create(TeacherService.class);

        //If no class is selected load all teachers, otherwise only they, who belong to the class
        if (classSpinner.getSelectedItemPosition() == 0) {
            ts.getTeachers(null).enqueue(new Callback<List<Personal>>() {
                @Override
                public void onResponse(Response<List<Personal>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        List<Personal> l = response.body();
                        if(l.isEmpty()){
                            List<String> fakeList = new ArrayList<>();
                            fakeList.add("Keinen Lehrer gefunden");
                            teacherSpinner.setAdapter(ArrayAdapterUtil.createAdapter(context,fakeList));
                            teacherSpinner.setEnabled(false);
                            return;
                        }
                        Collections.sort(l);
                        ArrayAdapter<Personal> adapter = ArrayAdapterUtil.createAdapter(context, l);
                        teacherSpinner.setAdapter(adapter);
                        teacherSpinner.setEnabled(true);
                        setHours();
                        setContact();

                    }
                }

                @Override
                public void onFailure(Throwable t) {}
            });

        } else {
            ts.getTeachers(klasse.getKlaBezeichung()).enqueue(new Callback<List<Personal>>() {
                @Override
                public void onResponse(Response<List<Personal>> response, Retrofit retrofit) {
                    if (response.isSuccess()) {
                        List<Personal> l = response.body();
                        if(l.isEmpty()){
                            List<String> fakeList = new ArrayList<>();
                            fakeList.add("Keinen Lehrer gefunden");
                            teacherSpinner.setAdapter(ArrayAdapterUtil.createAdapter(context,fakeList));
                            teacherSpinner.setEnabled(false);
                            return;
                        }
                        Collections.sort(l);
                        ArrayAdapter<Personal> adapter = ArrayAdapterUtil.createAdapter(context, l);
                        teacherSpinner.setAdapter(adapter);
                        teacherSpinner.setEnabled(true);
                        setHours();
                        setContact();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                }
            });
        }
    }

    /**
     * Load information about the AV.
     */
    private void getAV() {
        TeacherService ts = RetrofitInstance.getInstance().getBuilder().create(TeacherService.class);
        ts.getAv().enqueue(new Callback<Personal>() {
            @Override
            public void onResponse(Response<Personal> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    Personal p = response.body();
                    av_tel.setText(p.getTelefonnr());
                    av_email.setText(p.getEmail());
                }
            }

            @Override
            public void onFailure(Throwable t) {}
        });
    }

    /**
     * Sets the hours of the corresponding teacher.
     */
    private void setHours() {
        if(teacherSpinner.getSelectedItem() instanceof String){
            hours.setText("Bitte wähle einen Lehrer aus");
            return;
        }
        Personal p = (Personal) teacherSpinner.getSelectedItem();
        if (p == null) {
            hours.setText("Bitte wähle einen Lehrer aus");
            return;
        } else if(p.getSprechstundeCollection() == null || p.getSprechstundeCollection().isEmpty()){
            hours.setText("-");
            return;
        }
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < p.getSprechstundeCollection().size(); i++){
            sb.append(p.getSprechstundeCollection().get(i).toString());
            if(i != p.getSprechstundeCollection().size() - 1){
                sb.append("\n");
            }
        }
        hours.setText(sb.toString());
    }

    /**
     * Sets the contact information about the teacher.
     */
    private void setContact(){
        if(teacherSpinner.getSelectedItem() instanceof String){
            contact_email.setText("-");
            contact_tel.setText("-");
            return;
        }
        Personal p = (Personal) teacherSpinner.getSelectedItem();
        String email = "-";
        String tel = "-";
        if(p != null){
            if(!p.getEmail().isEmpty()){
                email = p.getEmail();
            }
            if(!p.getTelefonnr().isEmpty()){
                tel = p.getTelefonnr();
            }
        }

        contact_tel.setText(tel);
        contact_email.setText(email);
    }

}
