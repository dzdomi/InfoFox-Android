package at.htlstp.infofox_android.fragments.vmm;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import at.htlstp.infofox_android.MainTabActivity;
import at.htlstp.infofox_android.R;

/**
 * Every Tab consists of a root Fragment, which handles the backstack
 * of each of the tabs. It is only a placeholder for the currently
 * active fragment in this tab. It basically contains a container for
 * the active fragment in this tab.
 */
public class VmmRootFragment extends Fragment {

    public static final String LOG_TAG = VmmRootFragment.class.getSimpleName();

    private static FragmentManager manager;

    private View view;
    private Bundle bundle;

    public VmmRootFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        bundle = this.getArguments();
        VmmRootFragment.manager = getChildFragmentManager();

        //Remove all fragments, that are currently stored in the ChildfragmentManager
        int count = manager.getBackStackEntryCount();
        for(int i = 0; i < count; i++){
            manager.popBackStackImmediate();
        }
    }

    /**
     * Loads the first fragment into the root view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if(view  == null){
            view = inflater.inflate(R.layout.fragment_vmmroot, container, false);
        }

        if(savedInstanceState == null){
            SubjectsFragment sf = new SubjectsFragment();
            sf.setArguments(bundle);
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.grade_root, sf);
            transaction.commit();
        }

        return view;
    }

    /**
     * Checks if the current stack is empty. If it is empty
     * then the app will exit on the home screen. Otherwise it
     * pops ths stack back one fragment.
     */
    public void onBackPressed(){
        if(manager.getBackStackEntryCount() == 0){
            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addCategory(Intent.CATEGORY_HOME);
            //pretty dirty, but it gets the work done
            MainTabActivity.getMaincontext().startActivity(i);
        } else {
            manager.popBackStack();
        }
    }

    public static FragmentManager getManager() {
        return manager;
    }
}
