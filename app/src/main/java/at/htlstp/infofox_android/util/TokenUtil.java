package at.htlstp.infofox_android.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.NoSuchElementException;

import at.htlstp.infofox_android.LoginActivity;
import at.htlstp.infofox_android.MainTabActivity;
import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.database.InfoFoxSQLHelper;
import at.htlstp.infofox_android.models.ErrorMessage;
import at.htlstp.infofox_android.models.Token;
import at.htlstp.infofox_android.services.GCMTokenService;
import at.htlstp.infofox_android.services.TokenService;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Handles the InfoFox-Token and checks for regular updates, for example:
 * if the session is expired or the tokens needs to be refreshed. To only
 * have 1 instance of this object a singleton strategy is used.
 */
public class TokenUtil {

    public static final String LOG_TAG = TokenUtil.class.getSimpleName();

    private static TokenService service = RetrofitInstance.getInstance().getBuilder().create(TokenService.class);
    private static TokenUtil tokenUtil;

    private Token token;
    private Context context;
    private Callback<Void> delGCMCallBack = new Callback<Void>() {
        @Override
        public void onResponse(Response<Void> response, Retrofit retrofit) {}

        @Override
        public void onFailure(Throwable t) {}
    };

    private TokenUtil(Token token) {
        this.token = token;
    }

    /**
     * Returns a instance of TokenUtil.
     */
    public static TokenUtil getInstance() {
        if(tokenUtil == null){
            throw new IllegalStateException("TokenUtil wurde noch nicht initialisiert");
        }
        return tokenUtil;
    }

    /**
     * Tries to initialize the Token from the internal database.
     * If it is not stored a NoSuchElementException is thrown.
     */
    public static void initializeFromDatabase(Context context) throws NoSuchElementException{
        Token token = InfoFoxSQLHelper.getInstance().getInfoFoxToken();
        if(token == null){
            throw new NoSuchElementException("Token befindet sich nicht in der Datenbank");
        }
        tokenUtil = new TokenUtil(token);
        tokenUtil.context = context;
    }

    /**
     * Destroys the Singleton and deletes the InfoFox-Token from the Database.
     */
    public static void destroyInstance(){
        InfoFoxSQLHelper.getInstance().deleteInfoFoxToken();
        tokenUtil = null;
    }

    /**
     * Tries to get a token from the InfoFox-Server. If it succeds the Token will be stored
     * in the database. To return to the right function, the right callback will be called.
     */
    public static void initialize(final Context context, String user, String pass, final TokenCallback<Token> callback){
        service.getToken(user,pass).enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Response<Token> response, Retrofit retrofit) {
                if(response.isSuccess()){
                    tokenUtil = new TokenUtil(response.body());
                    tokenUtil.context = context;
                    tokenUtil.saveTokenToDisk();
                    callback.onSuccess(response);
                } else {
                    callback.onError(ErrorParser.parseError(response));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    /**
     * Returns the GCMToken. Asynchroniously the token is checked with the API and
     * if the token is valid, a refresh will be made for the token. This overhead, can
     * be accepted as the token is very small in comparison to the normally transferred data.
     */
    public Token getToken() {
        //Check the Token asynchron
        checkToken(new Callback() {
            @Override
            public void onResponse(Response response, Retrofit retrofit) {
                /**
                 * If the token is valid,
                 * try to refresh the token and save it to the disk
                 */
                if (response.isSuccess()) {
                    refresh(new Callback<Token>() {
                        @Override
                        public void onResponse(Response<Token> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                TokenUtil.this.token = response.body();
                                saveTokenToDisk();
                            } else {
                                Toast.makeText(context, "Fehler bei der Tokenanfrage", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {}
                    });
                    /**
                     * Otherwise delete the notification Token and
                     * send the token to the server, so he can delete it from
                     * his database. In addition the user get redirected to the Login Page.
                     */
                } else {
                    GCMTokenService service = RetrofitInstance.getInstance().getBuilder().create(GCMTokenService.class);
                    service.deleteToken(InfoFoxSQLHelper.getInstance().getGCMToken()).enqueue(delGCMCallBack);
                    InfoFoxSQLHelper.getInstance().deleteAllData();

                    Intent i = new Intent(context, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                    Toast.makeText(context, "Session abgelaufen", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {}
        });
        return token;
    }

    /**
     * Tries to get a new token from the API.
     */
    private void refresh(Callback<Token> callback) {
        service.refreshToken(token.getKey()).enqueue(callback);
    }

    /**
     * Checks the token with the api for validity.
     */
    private void checkToken(Callback callback){
        service.checkToken(token.getKey()).enqueue(callback);
    }

    /**
     * Saves the requested token in the Database.
     */
    private void saveTokenToDisk(){
        InfoFoxSQLHelper.getInstance().deleteAndInsertInfoFoxToken(this.token);
    }
}
