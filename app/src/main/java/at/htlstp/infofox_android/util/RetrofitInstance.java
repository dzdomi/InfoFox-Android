package at.htlstp.infofox_android.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by DZDomi on 04.11.2015.
 */
public class RetrofitInstance {

    private Retrofit builder;

    private static RetrofitInstance ourInstance;

    public static RetrofitInstance getInstance() {
        if(ourInstance == null){
            ourInstance = new RetrofitInstance();
        }
        return ourInstance;
    }

    private RetrofitInstance() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();
        builder = new Retrofit.Builder()
                                .addConverterFactory(GsonConverterFactory.create(gson))
                                .baseUrl(Constants.BASE_URL)
                                .build();

    }

    public Retrofit getBuilder() {
        return builder;
    }
}
