package at.htlstp.infofox_android.util;

/**
 * Created by DZDomi on 04.11.2015.
 */
public interface Constants {

    String PORT =  "8080";
    String PATH = "/InfoFox/api/";


    String BASE_URL = "http://gfhost.htlstp.ac.at" + PATH;

    //Genymotion IP
    //String BASE_URL = "http://10.0.3.2:" + PORT + PATH;


    //Test IP USB Debugging
    //String BASE_URL = "http://192.168.43.99:" + PORT + PATH;

    //DZDOMI-PC HOME
    //String BASE_URL = "http://10.0.0.175:" + PORT + PATH;

    //Raspi-TestServer
    //String BASE_URL = "http://dzdomi.no-ip.biz:" + PORT + PATH;

    //Macbook Hotspot
    //String BASE_URL = "http://172.20.10.3:" + PORT + PATH;

    //Macbook Home
    //String BASE_URL = "http://10.0.0.159:" + PORT + PATH;

    //Macbook Brunn
    //String BASE_URL = "http://192.168.1.96:" + PORT + PATH;

    //School IP Domi
    //String BASE_URL = "http://10.117.91.240:" + PORT + PATH;

    //Test IP USB Debugging Sebi Kolp
    //String BASE_URL = "http://192.168.1.108:" + PORT + PATH;

    //iMac Stefan Holzer
    //String BASE_URL = "http://10.0.0.2:" +PORT+ PATH;

    //Schul Laptop
    //String BASE_URL = "http://10.117.95.149:" + PORT + PATH;

    //Schule Sebi
    //String BASE_URL = "http://10.117.91.155:" + PORT + PATH;

    //Sebi zu Hause
    //String BASE_URL = "http://10.0.0.39:" + PORT + PATH;

    //Keys fuer die Intents
    String GRADES = "grades";
    String TUTORING = "tutoring";
    String SKIPPABLE = "skippable";

    //GCM
    String SENDER_ID = "467750832808";

    //EDV-Nummer Key fuer SharedPreferences
    String EDV_NUMMER = "edv_nummer";
}
