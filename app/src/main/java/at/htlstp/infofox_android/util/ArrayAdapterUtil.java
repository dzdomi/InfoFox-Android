package at.htlstp.infofox_android.util;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * This class creates some basic utilities for ArrayAdapter.
 */
public class ArrayAdapterUtil {

    /**
     * Creates an arrayadapter for the given list.
     * This is used to make the dropdown menues look the same throught the app.
     */
    public static ArrayAdapter createAdapter(Context context, List obj){
        if(context == null){
            return null;
        }
        ArrayAdapter adapter = new ArrayAdapter(context, android.R.layout.simple_spinner_item, obj);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }

}
