package at.htlstp.infofox_android.util;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;


/**
 * This class provides some Utilities to animate different views.
 */
public class AnimationUtil {

    /**
     * Expands a view to the given heigt, if its already collpased.
     */
    public static void expand(final View v, final int height) {

        final int targetHeight = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, v.getContext().getResources().getDisplayMetrics());

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1 ? targetHeight : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    /**
     * The counterpart of expand. It collpases a view until its completely hidden.
     */
    public static void collapse(final View v, final int height) {

        final int initialHeight = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, v.getContext().getResources().getDisplayMetrics());

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}
