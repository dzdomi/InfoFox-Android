package at.htlstp.infofox_android.util;

import at.htlstp.infofox_android.models.ErrorMessage;
import retrofit.Response;

/**
 * Token Callback, for Retrofit-Interface.
 */
public interface TokenCallback<T> {

    void onSuccess(Response<T> response);
    void onError(ErrorMessage message);
    void onFailure(Throwable t);
}
