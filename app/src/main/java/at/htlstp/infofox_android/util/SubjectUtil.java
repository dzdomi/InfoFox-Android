package at.htlstp.infofox_android.util;

import at.htlstp.infofox_android.R;

/**
 * Utility for subjects.
 */
public class SubjectUtil {

    /**
     * Returns the corresponding R value for
     * the given subject.
     */
    public static int getImageResource(String subject) {
        int imageResource = -1;
        switch (subject) {
            case "POS1":
            case "LOAL":
            case "POS1E":
                imageResource = R.drawable.pos;
                break;
            case "DBI1":
            case "DBI2":
                imageResource = R.drawable.dbi;
                break;
            case "SYP1":
                imageResource = R.drawable.syp;
                break;
            case "BWM":
            case "BWM3":
                imageResource = R.drawable.bwm;
                break;
            case "AM":
            case "MAM":
                imageResource = R.drawable.am;
                break;
            case "BESP":
                imageResource = R.drawable.besp;
                break;
            case "DUK":
            case "REKO":
            case "RHET":
            case "D":
                imageResource = R.drawable.deutsch;
                break;
            case "ETH":
            case "FEV":
            case "FIS":
            case "FRK":
            case "FR":
            case "R":
            case "RADV":
            case "RAK":
            case "RBAP":
            case "RBUD":
            case "RBUO":
            case "RCHG":
            case "RE":
            case "REFR":
            case "REHB":
            case "RF":
            case "RFCP":
            case "RGO":
            case "RGRK":
            case "RISL":
            case "RJLT":
            case "RK":
            case "RKO":
            case "RKPO":
            case "RMAZ":
            case "RMOS":
            case "RNAP":
            case "RPGG":
            case "RRO":
            case "RSO":
            case "RSOR":
            case "RUMO":
            case "RZJE":
                imageResource = R.drawable.rel;
                break;
            case "NVS1":
                imageResource = R.drawable.nvs;
                break;
            case "NW2":
                imageResource = R.drawable.nw;
                break;
            case "SOPK":
                imageResource = R.drawable.sopk;
                break;
            case "TC4A":
                imageResource = R.drawable.tc4a;
                break;
            case "GGP":
            case "GGPE":
                imageResource = R.drawable.ggp;
                break;
            case "E":
            case "E1":
                imageResource = R.drawable.en;
                break;
            case "TINF":
                imageResource = R.drawable.tinf;
                break;
            default:
                imageResource = R.drawable.unknown;
        }
        return imageResource;
    }


}
