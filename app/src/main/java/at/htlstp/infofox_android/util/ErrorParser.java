package at.htlstp.infofox_android.util;

import com.google.gson.Gson;

import java.io.IOException;

import at.htlstp.infofox_android.models.ErrorMessage;
import retrofit.Response;

/**
 * Utility for parsing error messages from the Retrofit-Response.
 */
public class ErrorParser {

    /**
     * Parses an error from the InfoFox-Api.
     */
    public static ErrorMessage parseError(Response body){
        try {
            return new Gson().fromJson(body.errorBody().charStream(), ErrorMessage.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
