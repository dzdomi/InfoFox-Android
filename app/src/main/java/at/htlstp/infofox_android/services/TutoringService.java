package at.htlstp.infofox_android.services;

import java.util.List;

import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Kompetenz;
import at.htlstp.infofox_android.models.Nachhilfe;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Documentation for this Services can be found on the InfoFox-API.
 */
public interface TutoringService {

    @GET("tutoring/")
    Call<List<Nachhilfe>> getTutoring(@Header("Authorization") String token, @Query("subject") String subject, @Query("year") Integer year, @Query("competence") List<Integer> competence, @Query("my")Boolean my);

    @POST("tutoring/")
    Call<Nachhilfe> insertTutoring(@Header("Authorization") String token, @Body Nachhilfe h);

    @PUT("tutoring/{id}")
    Call<Nachhilfe> updateTutoring(@Header("Authorization") String token, @Path("id") Integer id, @Body Nachhilfe h);

    @DELETE("tutoring/{id}")
    Call<Void> delTutoring(@Header("Authorization") String token, @Path("id") Integer id);

}
