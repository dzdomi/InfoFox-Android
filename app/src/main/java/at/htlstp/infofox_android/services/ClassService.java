package at.htlstp.infofox_android.services;

import java.util.List;

import at.htlstp.infofox_android.models.Klasse;
import at.htlstp.infofox_android.models.Personal;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;

/**
 * Documentation for this Services can be found on the InfoFox-API.
 */
public interface ClassService {

    @GET("classes")
    Call<List<Klasse>> getClasses();

    @GET("classes/student")
    Call<Klasse> getClassFromStudent(@Header("Authorization") String token);
}
