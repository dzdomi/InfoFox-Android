package at.htlstp.infofox_android.services;

import java.util.List;

import at.htlstp.infofox_android.models.Klasse;
import at.htlstp.infofox_android.models.Personal;
import at.htlstp.infofox_android.models.Sprechstunde;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Documentation for this Services can be found on the InfoFox-API.
 */
public interface TeacherService {

    @GET("teachers")
    Call<List<Personal>> getTeachers(@Query("class")String className);

    @GET("teachers/av")
    Call<Personal> getAv();

    @GET("teachers/{per_id}/hours")
    Call<List<Sprechstunde>> getSprechstunden(@Path("per_id") String per_id);

    @GET("teachers/{per_id}")
    Call<Personal> getTeacher(@Path("per_id") String per_id);

}
