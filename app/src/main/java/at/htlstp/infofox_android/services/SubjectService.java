package at.htlstp.infofox_android.services;

import java.util.List;

import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Kompetenz;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Documentation for this Services can be found on the InfoFox-API.
 */
public interface SubjectService {

    @GET("subjects/")
    Call<List<Gegenstand>> getSubjectsByYear(@Header("Authorization") String token, @Query("year")Integer year);

    @GET("subjects/{subject}/competence")
    Call<List<Kompetenz>> getAvailableCompetence(@Header("Authorization") String token, @Path("subject")String subject, @Query("year") Integer year);

}
