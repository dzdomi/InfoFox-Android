package at.htlstp.infofox_android.services;

import at.htlstp.infofox_android.models.Token;
import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Documentation for this Services can be found on the InfoFox-API.
 */
public interface TokenService {

    @POST("token")
    @FormUrlEncoded
    Call<Token> getToken(@Field("user") String username, @Field("pass") String password);

    @PUT("token/{token}/refresh/")
    Call<Token> refreshToken(@Path("token") String token);

    @GET("token/{token}/check/")
    Call<Token> checkToken(@Path("token") String token);
}
