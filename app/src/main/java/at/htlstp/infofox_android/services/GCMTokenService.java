package at.htlstp.infofox_android.services;

import at.htlstp.infofox_android.models.Token;
import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;

/**
 * This interface is used to save and delete the gcm
 * notification tokens on the InfoFox-Webservice.
 */
public interface GCMTokenService {

    @FormUrlEncoded
    @POST("gcmtoken")
    Call<Void> saveToken(@Header("authorization") String token, @Field("gcmtoken") String gcmtoken);

    @FormUrlEncoded
    @PUT("gcmtoken")
    Call<Void> deleteToken(@Field("gcmtoken") String gcmtoken);

}
