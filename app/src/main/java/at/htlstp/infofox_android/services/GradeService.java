package at.htlstp.infofox_android.services;

import java.util.List;

import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Laufendenote;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Path;

/**
 * Documentation for this Services can be found on the InfoFox-API.
 */
public interface GradeService {

    @GET("grades")
    Call<List<Gegenstand>> getGrades(@Header("Authorization") String token);

    @GET("grades/subject/{subject}/{teacher}")
    Call<List<Laufendenote>> getGradesFromSubject(@Header("Authorization") String token, @Path("subject") String subject, @Path("teacher") String teacher);
}
