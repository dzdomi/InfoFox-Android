package at.htlstp.infofox_android.gcm;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

import at.htlstp.infofox_android.database.InfoFoxSQLHelper;
import at.htlstp.infofox_android.models.Token;
import at.htlstp.infofox_android.services.GCMTokenService;
import at.htlstp.infofox_android.services.TokenService;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Handles all GCM-Tokens, which come from the RegistrationIntentService.
 */
public class RegistrationIntentService extends IntentService {

    private static final String LOG_TAG = RegistrationIntentService.class.getSimpleName();

    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    /**
     * Tries to read the SENDER ID from the Intent.
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            InstanceID instanceID = InstanceID.getInstance(this);
            String token = instanceID.getToken(Constants.SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            sendRegistrationToServer(token);

        } catch (Exception e) {
        }
    }

    /**
     * If there was a token in the database it will be deleted and sent to the server
     * so that the new token can be stored on the server. Additionally the new token will be
     * sent to the InfoFox-API for notifications.
     */
    private void sendRegistrationToServer(String gcmtoken) {
        GCMTokenService service = RetrofitInstance.getInstance().getBuilder().create(GCMTokenService.class);
        service.deleteToken(InfoFoxSQLHelper.getInstance().getGCMToken()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Response<Void> response, Retrofit retrofit) {}

            @Override
            public void onFailure(Throwable t) {}
        });
        InfoFoxSQLHelper.getInstance().deleteAndInsertGCMToken(gcmtoken);
        service.saveToken(TokenUtil.getInstance().getToken().getKey(),gcmtoken).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Response<Void> response, Retrofit retrofit) {}

            @Override
            public void onFailure(Throwable t) {}
        });
    }

}
