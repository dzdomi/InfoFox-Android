package at.htlstp.infofox_android.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.PowerManager;
import android.renderscript.RenderScript;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import at.htlstp.infofox_android.LoginActivity;
import at.htlstp.infofox_android.MainTabActivity;
import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.adapter.SubjectAdapter;
import at.htlstp.infofox_android.database.InfoFoxSQLHelper;
import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Laufendenote;
import at.htlstp.infofox_android.models.Token;
import at.htlstp.infofox_android.services.GradeService;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.RetrofitInstance;
import at.htlstp.infofox_android.util.SubjectUtil;
import at.htlstp.infofox_android.util.TokenUtil;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * This class gets all the packages from the InfoFox-Server
 * and creates a notification, which will lead back to the InfoFox-App.
 */
public class GradeGcmListenerService extends GcmListenerService {

    private static String LOG_TAG = GradeGcmListenerService.class.getSimpleName();

    /**
     * Gets called if a notification is send from the server to
     * GCM and will trigger a new notification for the user.
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String bemerkung = data.getString("lnt_bemerkung");
        String bewertung = data.getString("lnt_bewertung");
        String gegenstand = data.getString("geg_kurzbez");
        String lehrer = data.getString("per_kb");

        //Converts the image into a white one
        Drawable wrappedDrawable = DrawableCompat.wrap(getResources().getDrawable(SubjectUtil.getImageResource(gegenstand)));
        DrawableCompat.setTint(wrappedDrawable, getResources().getColor(android.R.color.white));

        //Builds the notification which will be displayed
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification_fox)
                        .setContentTitle("InfoFox")
                        .setContentText(bemerkung + ": " + bewertung)
                        .setCategory(Notification.CATEGORY_SERVICE)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setWhen(0)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setDefaults(Notification.DEFAULT_VIBRATE)
                        .setLights(Color.rgb(241, 141, 43), 1000, 1000)
                        .setDefaults(Notification.FLAG_SHOW_LIGHTS)
                        .setLargeIcon(drawableToBitmap(wrappedDrawable));


        // Creates an explicit intent back to our LoginActivity
        Intent resultIntent = new Intent(this, LoginActivity.class);
        resultIntent.putExtra(Constants.GRADES, gegenstand + ";" + lehrer);


        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(LoginActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        //Calls the NotificationManager and acquires the wakelock from the phone in order to display the notification
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");

        wl.acquire(15000);
        mNotificationManager.notify(2, mBuilder.build());
    }

    /**
     * Converts a drawable into a bitmap image.
     */
    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

}
