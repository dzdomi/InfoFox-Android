package at.htlstp.infofox_android;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import at.htlstp.infofox_android.fragments.teacher.TeacherFragment;

/**
 * Shows the teacher information, if the user
 * skips the login.
 */
public class SkipActivity extends AppCompatActivity {

    public final static String LOG_TAG = SkipActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_skip);
        Toolbar t = (Toolbar)findViewById(R.id.skip_toolbar);
        setSupportActionBar(t);
        setStatusBarColor();
        getSupportFragmentManager().beginTransaction()
                            .replace(R.id.placeholder_skip, new TeacherFragment())
                            .commit();
    }


    private void setStatusBarColor() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = this.getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            w.setStatusBarColor(this.getResources().getColor(R.color.status_bar));
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

