package at.htlstp.infofox_android.models;

import java.io.Serializable;
import java.util.Date;

public class Laufendenote implements Serializable, Comparable<Laufendenote> {

    private String lntBemerkung;
    private String lntBewertung;
    private Double lntRealBewertung;
    private boolean lntSetze5;
    private boolean lntGelesen;
    private Date lntTimestamp;

    public Laufendenote() {
    }

    public Laufendenote(String lntBemerkung, String lntBewertung, Double lntRealBewertung, boolean lntSetze5, boolean lntGelesen, Date lntTimestamp) {
        this.lntBemerkung = lntBemerkung;
        this.lntBewertung = lntBewertung;
        this.lntRealBewertung = lntRealBewertung;
        this.lntSetze5 = lntSetze5;
        this.lntGelesen = lntGelesen;
        this.lntTimestamp = lntTimestamp;
    }

    public String getLntBemerkung() {
        return lntBemerkung;
    }

    public void setLntBemerkung(String lntBemerkung) {
        this.lntBemerkung = lntBemerkung;
    }

    public String getLntBewertung() {
        return lntBewertung;
    }

    public void setLntBewertung(String lntBewertung) {
        this.lntBewertung = lntBewertung;
    }

    public Double getLntRealBewertung() {
        return lntRealBewertung;
    }

    public void setLntRealBewertung(Double lntRealBewertung) {
        this.lntRealBewertung = lntRealBewertung;
    }

    public boolean getlntGelesen() {
        return lntGelesen;
    }

    public void setlntGelesen(boolean lntGelesen) {
        this.lntGelesen = lntGelesen;
    }

    public Date getLntTimestamp() {
        return lntTimestamp;
    }

    public void setLntTimestamp(Date lntTimestamp) {
        this.lntTimestamp = lntTimestamp;
    }
    
    public boolean getLntSetze5() {
        return lntSetze5;
    }

    public void setLntSetze5(boolean lntSetze5) {
        this.lntSetze5 = lntSetze5;
    }

    @Override
    public String toString() {
        return "Laufendenote{" +
                "lntBemerkung='" + lntBemerkung + '\'' +
                ", lntBewertung='" + lntBewertung + '\'' +
                ", lntRealBewertung=" + lntRealBewertung +
                ", lntSetze5=" + lntSetze5 +
                ", lntGelesen=" + lntGelesen +
                ", lntTimestamp=" + lntTimestamp +
                '}';
    }

    @Override
    public int compareTo(Laufendenote another) {
        return another.getLntTimestamp().compareTo(this.getLntTimestamp());
    }
}
