package at.htlstp.infofox_android.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Klasse implements Serializable, Comparable<Klasse>{
    public Klasse(){

    }

    public Klasse(String klaBezeichung){
        this.klaBezeichung = klaBezeichung;
    }

    @SerializedName("klaBezeichnung")
    private String klaBezeichung;

    public String getKlaBezeichung() {
        return klaBezeichung;
    }

    public void setKlaBezeichung(String klaBezeichung) {
        this.klaBezeichung = klaBezeichung;
    }


    @Override
    public int compareTo(Klasse another) {
        return this.klaBezeichung.compareTo(another.getKlaBezeichung());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Klasse klasse = (Klasse) o;

        return !(klaBezeichung != null ? !klaBezeichung.equals(klasse.klaBezeichung) : klasse.klaBezeichung != null);

    }

    @Override
    public int hashCode() {
        return klaBezeichung != null ? klaBezeichung.hashCode() : 0;
    }

    @Override
    public String toString() {
        return this.klaBezeichung;
    }
}
