package at.htlstp.infofox_android.models;

import java.io.Serializable;

public class LoginData implements Serializable {
    private String user;
    private String pass;

    public LoginData() {
    }

    public LoginData(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
