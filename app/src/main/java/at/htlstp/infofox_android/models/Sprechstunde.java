package at.htlstp.infofox_android.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Sprechstunde implements Serializable {

    @SerializedName("sprStr")
    private String sprStr;

    public Sprechstunde(){}

    public String getSprStr() {
        return sprStr;
    }

    public void setSprStr(String sprStr) {
        this.sprStr = sprStr;
    }

    @Override
    public String toString() {
        return this.sprStr;
    }
}