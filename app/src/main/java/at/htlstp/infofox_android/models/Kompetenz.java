package at.htlstp.infofox_android.models;

import java.io.Serializable;

public class Kompetenz implements Serializable {
    private int komId;
    private int komJahrgang;
    private String komBezeichnung;
    private Gegenstand gegKurzbez;

    public Kompetenz(int komId, int komJahrgang, String komBezeichnung, Gegenstand gegKurzbez) {
        this.komId = komId;
        this.komJahrgang = komJahrgang;
        this.komBezeichnung = komBezeichnung;
        this.gegKurzbez = gegKurzbez;
    }

    public int getKomId() {
        return komId;
    }

    public void setKomId(int komId) {
        this.komId = komId;
    }

    public int getKomJahrgang() {
        return komJahrgang;
    }

    public void setKomJahrgang(int komJahrgang) {
        this.komJahrgang = komJahrgang;
    }

    public String getKomBezeichnung() {
        return komBezeichnung;
    }

    public void setKomBezeichnung(String komBezeichnung) {
        this.komBezeichnung = komBezeichnung;
    }

    public Gegenstand getGegKurzbez() {
        return gegKurzbez;
    }

    public void setGegKurzbez(Gegenstand gegKurzbez) {
        this.gegKurzbez = gegKurzbez;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Kompetenz kompetenz = (Kompetenz) o;

        return komId == kompetenz.komId;

    }

    @Override
    public int hashCode() {
        return komId;
    }

    @Override
    public String toString() {
        return komBezeichnung;
    }
}
