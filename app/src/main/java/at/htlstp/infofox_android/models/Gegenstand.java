package at.htlstp.infofox_android.models;

import java.io.Serializable;
import java.util.List;

public class Gegenstand implements Serializable {

    private String gegKurzbez;
    private String gegLangbez;
    private String teacher;

    private List<Laufendenote> grades;

    public Gegenstand() {
    }

    public Gegenstand(String gegLangbez){
        this.gegLangbez = gegLangbez;
    }

    public Gegenstand(String gegKurzbez, String gegLangbez, String teacher){
        this.gegKurzbez = gegKurzbez;
        this.gegLangbez = gegLangbez;
        this.teacher = teacher;
    }

    public String getGegKurzbez() {
        return gegKurzbez;
    }

    public void setGegKurzbez(String gegKurzbez) {
        this.gegKurzbez = gegKurzbez;
    }

    public String getGegLangbez() {
        return gegLangbez;
    }

    public void setGegLangbez(String gegLangbez) {
        this.gegLangbez = gegLangbez;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public List<Laufendenote> getGrades() {
        return grades;
    }

    public void setGrades(List<Laufendenote> grades) {
        this.grades = grades;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gegenstand that = (Gegenstand) o;

        if (!gegKurzbez.equals(that.gegKurzbez)) return false;
        return teacher.equals(that.teacher);

    }

    @Override
    public int hashCode() {
        int result = gegKurzbez.hashCode();
        result = 31 * result + teacher.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return this.gegLangbez;
    }
}