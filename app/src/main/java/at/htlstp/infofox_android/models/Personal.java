package at.htlstp.infofox_android.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public class Personal implements Serializable, Comparable<Personal> {

    @SerializedName("perVorname")
    private String perVorname;

    @SerializedName("perZuname")
    private String perZuname;

    @SerializedName("titel")
    private Titel titel;

    @SerializedName("perEmail")
    private String email;

    @SerializedName("perTelefonnr")
    private String telefonnr;

    @SerializedName("hours")
    private List<Sprechstunde> sprechstundeCollection;

    public Personal(){

    }

    @Override
    public int compareTo(Personal another) {
        return this.perZuname.compareTo(another.getPerZuname());
    }

    class Titel implements Serializable {
        @SerializedName("titKurbez")
        private String titel;

        public Titel(){}

        @Override
        public String toString() {
            return titel;
        }
    }

    public String getPerVorname() {
        return perVorname;
    }

    public void setPerVorname(String perVorname) {
        this.perVorname = perVorname;
    }

    public Titel getTitel() {
        return titel;
    }

    public void setTitel(Titel titel) {
        this.titel = titel;
    }

    public String getPerZuname() {
        return perZuname;
    }

    public void setPerZuname(String perZuname) {
        this.perZuname = perZuname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(email == null || email.isEmpty()){
            this.email = "keine E-Mail vorhanden!";
        }else{
            this.email = email;
        }

    }

    public String getTelefonnr() {
        return telefonnr;
    }

    public void setTelefonnr(String telefonnr) {
        if(telefonnr == null || telefonnr.isEmpty()){
            this.telefonnr = "keine Telefonnummer vorhanden!";
        }else{
            this.telefonnr = telefonnr;
        }
    }

    @Override
    public String toString() {
        return titel.toString()+" " +this.getPerZuname()+" "+this.getPerVorname();
    }

    public List<Sprechstunde> getSprechstundeCollection() {
        return sprechstundeCollection;
    }

    public void setSprechstundeCollection(List<Sprechstunde> sprechstundeCollection) {
        this.sprechstundeCollection = sprechstundeCollection;
    }

    public String info(){
        return "Telefonnummer: "+this.telefonnr+"\n"
                + "Email: " +email;

    }
}
