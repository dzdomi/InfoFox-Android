package at.htlstp.infofox_android.models;


import java.io.Serializable;
import java.util.Date;

public class Token implements Serializable {
    private String key;
    private Date valid;

    public Token () {}

    public Token(String key, Date valid) {
        this.key = key;
        this.valid = valid;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Date getValid() {
        return valid;
    }

    public void setValid(Date valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "Token{" +
                "key='" + key + '\'' +
                ", valid=" + valid +
                '}';
    }
}
