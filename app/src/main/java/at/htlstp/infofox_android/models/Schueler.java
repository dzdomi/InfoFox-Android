package at.htlstp.infofox_android.models;

import java.io.Serializable;

public class Schueler implements Serializable {
    private int ssdId;
    private String ssdVorname;
    private String ssdZuname;
    private Klasse klaBezeichnung;

    public Schueler(int ssdId, String ssdVorname, String ssdZuname, Klasse klaBezeichnung) {
        this.ssdId = ssdId;
        this.ssdVorname = ssdVorname;
        this.ssdZuname = ssdZuname;
        this.klaBezeichnung = klaBezeichnung;
    }

    public int getSsdId() {
        return ssdId;
    }

    public void setSsdId(int ssdId) {
        this.ssdId = ssdId;
    }

    public String getSsdVorname() {
        return ssdVorname;
    }

    public void setSsdVorname(String ssdVorname) {
        this.ssdVorname = ssdVorname;
    }

    public String getSsdZuname() {
        return ssdZuname;
    }

    public void setSsdZuname(String ssdZuname) {
        this.ssdZuname = ssdZuname;
    }

    public Klasse getKlaBezeichnung() {
        return klaBezeichnung;
    }

    public void setKlaBezeichnung(Klasse klaBezeichnung) {
        this.klaBezeichnung = klaBezeichnung;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Schueler schueler = (Schueler) o;

        return ssdId == schueler.ssdId;

    }

    @Override
    public int hashCode() {
        return ssdId;
    }

    @Override
    public String toString() {
        return "Schueler{" +
                "ssdId=" + ssdId +
                ", ssdVorname='" + ssdVorname + '\'' +
                ", ssdZuname='" + ssdZuname + '\'' +
                ", klaBezeichnung=" + klaBezeichnung +
                '}';
    }
}
