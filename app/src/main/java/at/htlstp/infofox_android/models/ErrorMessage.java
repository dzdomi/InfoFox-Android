package at.htlstp.infofox_android.models;

import java.io.Serializable;

public class ErrorMessage implements Serializable {

    private Integer error_code;
    private String desc;

    public ErrorMessage() {
    }

    public ErrorMessage(Integer error_code, String desc) {
        this.error_code = error_code;
        this.desc = desc;
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "error_code=" + error_code +
                ", desc='" + desc + '\'' +
                '}';
    }
}