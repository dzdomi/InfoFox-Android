package at.htlstp.infofox_android.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


public class Nachhilfe implements Serializable, Comparable<Nachhilfe> {
    private Integer nachId;
    private Date nachDatum;
    private String nachKommentar;
    private Gegenstand gegenstand;
    @SerializedName("kompetenzBereiche")
    private List<Kompetenz> kompetenz;
    private Schueler schueler;
    private Integer nachJahrgang;

    //Only used for sending the object, otherwisse ignore these attributes
    private String subject;
    private List<Integer> komIds;

    public Nachhilfe(){}

    public Nachhilfe(int nachId, Date nachDatum, String nachKommentar, Gegenstand gegenstand, List<Kompetenz> kompetenz, Schueler schueler) {
        this.nachId = nachId;
        this.nachDatum = nachDatum;
        this.nachKommentar = nachKommentar;
        this.gegenstand = gegenstand;
        this.kompetenz = kompetenz;
        this.schueler = schueler;
    }

    public void setKomIds(List<Integer> komIds) {
        this.komIds = komIds;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Integer getNachJahrgang() {
        return nachJahrgang;
    }

    public void setNachJahrgang(Integer nachJahrgang) {
        this.nachJahrgang = nachJahrgang;
    }

    public Integer getNachId() {
        return nachId;
    }

    public void setNachId(Integer nachId) {
        this.nachId = nachId;
    }

    public Date getNachDatum() {
        return nachDatum;
    }

    public void setNachDatum(Date nachDatum) {
        this.nachDatum = nachDatum;
    }

    public String getNachKommentar() {
        return nachKommentar;
    }

    public void setNachKommentar(String nachKommentar) {
        this.nachKommentar = nachKommentar;
    }

    public Gegenstand getGegenstand() {
        return gegenstand;
    }

    public void setGegenstand(Gegenstand gegenstand) {
        this.gegenstand = gegenstand;
    }

    public List<Kompetenz> getKompetenz() {
        return kompetenz;
    }

    public void setKompetenz(List<Kompetenz> kompetenz) {
        this.kompetenz = kompetenz;
    }

    public Schueler getSchueler() {
        return schueler;
    }

    public void setSchueler(Schueler schueler) {
        this.schueler = schueler;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Nachhilfe nachhilfe = (Nachhilfe) o;

        return nachId == nachhilfe.nachId;

    }

    @Override
    public int hashCode() {
        return nachId;
    }

    @Override
    public String toString() {
        return "Nachhilfe{" +
                "nachId=" + nachId +
                ", nachDatum=" + nachDatum +
                ", nachKommentar='" + nachKommentar + '\'' +
                ", gegenstand=" + gegenstand +
                ", kompetenz=" + kompetenz +
                ", schueler=" + schueler +
                '}';
    }

    @Override
    public int compareTo(Nachhilfe another) {
        return another.nachDatum.compareTo(this.nachDatum);
    }
}
