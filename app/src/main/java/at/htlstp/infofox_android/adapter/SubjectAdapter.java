package at.htlstp.infofox_android.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.database.InfoFoxSQLHelper;
import at.htlstp.infofox_android.fragments.vmm.GradesFragment;
import at.htlstp.infofox_android.fragments.vmm.VmmRootFragment;
import at.htlstp.infofox_android.models.Gegenstand;
import at.htlstp.infofox_android.models.Laufendenote;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.SubjectUtil;

/**
 * Holds the different subjects in the RecyclerView on the
 * Main VMM Fragment. It uses a ViewHolder for each of the subjects.
 */
public class SubjectAdapter extends RecyclerView.Adapter<SubjectAdapter.ViewHolder> {

    private List<Gegenstand> gegenstaende;
    private Fragment fragment;

    private View view;

    public SubjectAdapter(Fragment fragment, List<Gegenstand> gegenstaende){
        this.fragment = fragment;
        this.gegenstaende = gegenstaende;
    }

    /**
     * If the view is getting created this method will get executed.
     * It loads the layout from the xml file and adds a slow animation for
     * the loading of the view.
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_cardview, parent, false);
        Animation anim = AnimationUtils.loadAnimation(fragment.getActivity(), R.anim.scaletofullsize);
        anim.setDuration(400);
        view.startAnimation(anim);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    /**
     * This method is called with the corresponding ViewHolder
     * and sets the values of subject in the ViewHolder.
     * For example: Name and Image
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String subject_short = this.gegenstaende.get(position).getGegKurzbez();
        String teacher = this.gegenstaende.get(position).getTeacher();
        holder.subject_text.setText(subject_short + " - " + teacher);
        holder.subject_image.setImageResource(SubjectUtil.getImageResource(subject_short));
        //Check if there is any unread grade (blue circle)
        for(Laufendenote grade : this.gegenstaende.get(position).getGrades()){
            if(!grade.getlntGelesen()){
                holder.blue_circle.setVisibility(View.VISIBLE);
                break;
            }
        }

    }

    @Override
    public int getItemCount() {
        return this.gegenstaende.size();
    }

    /**
     * This class holds the view for each grade. It basically consists
     * of the name and the image of the subject.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView subject_text;
        public ImageView subject_image;
        public View blue_circle;

        public ViewHolder(View view){
            super(view);

            subject_text = (TextView) view.findViewById(R.id.subject_short);
            subject_image = (ImageView) view.findViewById(R.id.subject_image);
            blue_circle = view.findViewById(R.id.subject_new_grade);

            /**
             * If the view is clicked, then we have to update the read annotation
             * in the database and also set the fragment to the given subject.
             */
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(blue_circle.getVisibility() == View.VISIBLE){
                        blue_circle.setVisibility(View.GONE);
                    }
                    InfoFoxSQLHelper.getInstance().updateRead(SubjectAdapter.this.gegenstaende.get(getAdapterPosition()));
                    FragmentTransaction transaction = VmmRootFragment.getManager().beginTransaction();
                    GradesFragment gf = new GradesFragment();
                    Bundle b = new Bundle();
                    b.putSerializable(Constants.GRADES, SubjectAdapter.this.gegenstaende.get(getAdapterPosition()));
                    gf.setArguments(b);
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    transaction.replace(R.id.grade_root, gf, "grades");
                    transaction.addToBackStack("grades");
                    transaction.commit();
                }
            });
        }
    }

}
