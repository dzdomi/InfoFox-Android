package at.htlstp.infofox_android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.fragments.nachhilfe.TutoringEditDetailFragment;
import at.htlstp.infofox_android.fragments.nachhilfe.TutoringRootFragment;
import at.htlstp.infofox_android.fragments.nachhilfe.TutoringShowFragment;
import at.htlstp.infofox_android.models.Nachhilfe;
import at.htlstp.infofox_android.util.Constants;
import at.htlstp.infofox_android.util.SubjectUtil;

/**
 * Holds the tutoring objects for the corresponding ListView.
 */
public class NachhilfeAdapter extends BaseAdapter {

    private final Context context;

    private TextView textview_pupil;
    private TextView textview_subject;
    private ImageView image_subject;

    private List<Nachhilfe> listNachhilfe;

    private int lastPosition = -1;
    private boolean isMoving = false;

    private boolean edit;

    public NachhilfeAdapter(Context context, List<Nachhilfe> listNachhilfe, Boolean edit) {
        this.context = context;
        this.listNachhilfe = listNachhilfe;
        this.edit = edit;
    }

    @Override
    public int getCount() {
        return listNachhilfe.size();
    }

    @Override
    public Nachhilfe getItem(int i) {
        return listNachhilfe.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.list_item_nachhilfe, viewGroup, false);
        }
        Nachhilfe n = listNachhilfe.get(i);
        textview_pupil = (TextView) view.findViewById(R.id.nachhilfe_item_text);
        image_subject = (ImageView) view.findViewById(R.id.nachhilfe_item_image);
        textview_subject = (TextView) view.findViewById(R.id.nachhilfe_subject);

        textview_subject.setTextColor(context.getResources().getColor(R.color.button_color));
        textview_pupil.setTextColor(Color.BLACK);
        image_subject.setImageDrawable(context.getResources().getDrawable(SubjectUtil.getImageResource(n.getGegenstand().getGegKurzbez())));

        StringBuilder sb = new StringBuilder(n.getSchueler().getSsdVorname());
        sb.append(" ");
        sb.append(n.getSchueler().getSsdZuname());
        sb.append(" (");
        sb.append(n.getSchueler().getKlaBezeichnung().getKlaBezeichung());
        sb.append("), ");
        sb.append(n.getNachJahrgang());
        sb.append(". Jahrgang");

        textview_subject.setText(n.getGegenstand().getGegLangbez());
        textview_pupil.setText(sb.toString());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = TutoringRootFragment.getManager().beginTransaction();
                Fragment frag = null;
                //If item is clicked, should it enter the edit fragment or the show fragment
                if(edit){
                    frag = new TutoringEditDetailFragment();
                } else {
                    frag = new TutoringShowFragment();
                }
                Bundle b = new Bundle();
                b.putSerializable(Constants.TUTORING, NachhilfeAdapter.this.listNachhilfe.get(i));
                frag.setArguments(b);
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                transaction.replace(R.id.nachhilfe_root, frag, "nachhilfeDetail");
                transaction.addToBackStack("nachhilfeDetail");
                transaction.commit();
            }
        });

        //Make the list more fluent with some awesome animations! YEAH!
        if(isMoving){
            Animation animation = AnimationUtils.loadAnimation(context, (i > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
            view.startAnimation(animation);
        }
        lastPosition = i;

        return view;
    }

    public void setIsMoving(boolean isMoving) {
        this.isMoving = isMoving;
    }
}
