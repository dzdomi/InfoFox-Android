package at.htlstp.infofox_android.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import at.htlstp.infofox_android.R;
import at.htlstp.infofox_android.models.Laufendenote;

/**
 * Holds the values in the corresponding ListView.
 */
public class GradesAdapter extends BaseAdapter  {

    private final Context context;
    private List<Laufendenote> noten;

    public GradesAdapter(Context context, List<Laufendenote> noten){
        this.context = context;
        this.noten = noten;
    }

    @Override
    public int getCount() {
        return noten.size();
    }

    @Override
    public Laufendenote getItem(int position) {
        return noten.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_grade, parent, false);
        }
        RelativeLayout inside = (RelativeLayout) convertView.findViewById(R.id.grades_layout_inside);
        if(position % 2 == 0){
            inside.setBackgroundColor(context.getResources().getColor(android.R.color.white));
        } else {
            inside.setBackgroundColor(context.getResources().getColor(R.color.grades_listview_background_grey));
        }

        TextView grade_date = (TextView) convertView.findViewById(R.id.grade_date);
        TextView grade_bemerkung = (TextView) convertView.findViewById(R.id.grade_bemerkung);
        TextView grade_bewertung = (TextView) convertView.findViewById(R.id.grade_bewertung);
        ImageView grade_image = (ImageView) convertView.findViewById(R.id.grade_image);

        Calendar cal = Calendar.getInstance();
        cal.setTime(noten.get(position).getLntTimestamp());
        //Month + 1 because Months are indexed by 0, WTF Java Devs what were you thinking
        grade_date.setText(cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH) + 1) + "." + cal.get(Calendar.YEAR));
        grade_bemerkung.setText(noten.get(position).getLntBemerkung());
        grade_bewertung.setText("" + noten.get(position).getLntBewertung());
        grade_image.setImageResource(getImageResource(noten.get(position).getLntBewertung()));

        return convertView;
    }

    /**
     * Returns the R id for the color image
     * of the given rating.
     */
    private int getImageResource(String bewertung){
        int imageresource = -1;
        double parsedValue = 0;
        try{
            parsedValue = Double.parseDouble(bewertung);
        } catch(NumberFormatException e){
            switch (bewertung){
                case "+": imageresource = R.drawable.n1;
                    break;
                case "+~":
                case "~+": imageresource = R.drawable.n2;
                    break;
                case "~": imageresource = R.drawable.n3;
                    break;
                case "~-":
                case "-~": imageresource = R.drawable.n4;
                    break;
                case "-": imageresource = R.drawable.n5;
                    break;
            }
            return imageresource;
        }
        if(parsedValue < 1.5){
            imageresource = R.drawable.n1;
        } else if(parsedValue >= 1.5 && parsedValue < 2.5){
            imageresource = R.drawable.n2;
        } else if(parsedValue >= 2.5 && parsedValue < 3.5){
            imageresource = R.drawable.n3;
        } else if(parsedValue >= 3.5 && parsedValue < 4.5){
            imageresource = R.drawable.n4;
        } else {
            imageresource = R.drawable.n5;
        }
        return imageresource;
    }
}
